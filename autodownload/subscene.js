
var request = require('request');
var cheerio = require('cheerio')
var http = require('http');
var https = require('https');
var urlEndocde = require('url');
var dateFormat = require('dateformat');
var ggsheet = require('./GGSheet');
const fs = require('fs');
const readline = require('readline');
const google = require('googleapis');

//var sleep = require('sleep');

var urlSubsceneQuery = "https://subscene.com/subtitles/title?q=";
var urlSubScene = "https://subscene.com";
var logStream = fs.createWriteStream('subscenes.txt', { 'flags': 'w' });
var preParsePage = 0;

//addToIDM("http://85.25.211.138/downloads/HDPOPCORNS.Blade-Runner-2049-2017-720p.mp4?st=Av71UTtiFf4Lk9n8NAOubA&e=1515587327","1231 20180108v1");
ggsheet.repareAuthorize(listMajors);


function cc(strurl,tmvdbID, movieName, year, next)
{
  var options = {
    url: '' + strurl,
    timeout: 10000
}
  return request(options, function (error, response, html) {
    if (!error && response.statusCode == 200) {
      var $ = cheerio.load(html);

      if(tmvdbID =="8844")
        var k =0;
      var ListdemoLink = $('.byTitle .search-result .title').children('a');
        if (typeof(ListdemoLink) != "undefined")
        {
          for (var i = 0; i< ListdemoLink.length ;i++)
          {
          //  var attribs =ListdemoLink[i].attribs;
        // console.log(" ------------- urlSubScene = " + i);
            if (typeof(ListdemoLink[i].attribs) != "undefined")
            {
              var sublink = ListdemoLink[i].attribs.href;
              var name = ListdemoLink[i].children[0].data;
              if (sublink && name 
                && name.toLocaleLowerCase().indexOf(movieName.toLocaleLowerCase()) !== -1
                && name.toLocaleLowerCase().indexOf(year.toLocaleLowerCase()) !== -1
               && ((movieName.split(" ").length == name.split(" ").length -1 ))// ||  name.split(" ").length/2 <= movieName.split(" ").length)
              )
                {
                  console.log(" ------------- urlSubScene = tmvdbID : " + tmvdbID +" " + urlSubScene + sublink);
                  console.log(" ------------- urlSubScene = year : " + year +" " + urlSubScene + sublink);
                
                  var row = tmvdbID + "," + urlSubScene + sublink;
                  logStream.write(row + "\n");
                 // sleep.msleep(2000);
                  return next(null);
                }
            }

          }
      } 
     // sleep.msleep(5000);  
     console.log(" ------------- response.statusCode ------------------- = "+ response.statusCode) ;  
     if(preParsePage < 5)
     {
       preParsePage ++;
     // sleep.msleep(500);
       return cc(strurl,tmvdbID, movieName, year, next);
     } 
      return next(null);
    }
    else
    {
      console.log(" ------------- error ------------------- = ");
      if(preParsePage < 5)
      {
        preParsePage ++;
        //sleep.msleep(3000);
        return cc(strurl,tmvdbID, movieName, year, next);
      } 
      return next(null);
    }

  });
}

function parse(tmvdbID, movieName, year, next) {
  // movieName = "Inglourious Basterds";
 // year = "2009";
  var url = "https://subscene.com/subtitles/title?q=" + movieName;
  console.log("begin parse " + year +" " + url);

  preParsePage = 0;
  cc(url,tmvdbID, movieName, year, next);
  
}

function listMajors(auth) {
  var sheets = google.sheets('v4');
  sheets.spreadsheets.values.get({
    auth: auth,
    spreadsheetId: '19G_UTLVOO1Ils-I809VMLjVyUCjlmBjPpOHygIQfs9I',
    range: 'Sheet49!A1:S',
  }, function (err, response) {
    if (err) {
      console.log('The API returned an error: ' + err);
      return;
    }
    var data = response.values;
    if (data.length == 0) {
      console.log('No data found.');
    } else {

      var fs = require('fs');
      var logStream = fs.createWriteStream('log.txt', { 'flags': 'w' });
      // use {'flags': 'a'} to append and {'flags': 'w'} to erase and write a new file
      var temp = 0;
      var header = data[0];

      var listMovies = [];
      for (var i = 1; i < data.length; i++) {
        var row = data[i];
        var tmvdbID = parseInt(row[0]);
        var name = row[1];
        var year = row[2].split("-")[0];;// row[2].split("-")[0];
        var sub = null;//row[4];

        console.log("tmvdbID = " + tmvdbID);
        console.log("name = " + name);
        console.log("year = " + year);
        if(sub == null || sub == ""  || sub.indexOf("subscene") == -1 )
        {
          listMovies.push({ tmvdbID: tmvdbID, name: name , year : year });
        } else {
          console.log("co roi down cai noi : " + sub);
        }
      }
      
      function* genNextMovie() {
        for (var i = 0; i < listMovies.length; i++) {
          var element = listMovies[i];
          yield element;
        }
        return null;
      }

      var logStream = fs.createWriteStream('downloadLink.txt', { 'flags': 'w' });

      var movielinklist = genNextMovie();
      var movieLink = movielinklist.next().value;
      parse(movieLink.tmvdbID, movieLink.name, movieLink.year, saveDownloadLinkToFile);
      function saveDownloadLinkToFile(link,name,extension) {
        if(link)
        {
          console.log('get link = ' + link);
          console.log('get name = ' + name);
          console.log('get extension = ' + extension);
          logStream.write(link + "\n");
          addToIDM(link,name,extension);
        }

        var nextLink = movielinklist.next().value;
        if(nextLink)
          return parse(nextLink.tmvdbID, nextLink.name, nextLink.year, saveDownloadLinkToFile);
        else
        {
          console.log('Import Done!');
          return null;
        }
      }
      
    }
  });
}

function getDownloadPage(tmvdbID, url, next) {

  console.log("begin getDownloadPage " + url);
  return request(url, function (error, response, html) {
    if (!error && response.statusCode == 200) {
      var $ = cheerio.load(html);
      var form = $('form').filter(function (i, el) {
        return $(this).attr('action') === '/select-movie-quality.php';
      });
      var inputFields = form.children('input');
      var options = {};
      for (var i = 0; i < inputFields.length; i++) {
        var key = inputFields[i].attribs.name;
        var value = inputFields[i].attribs.value;
        options[key] = value;
      }
      return getDownloadLink(tmvdbID, options, next);
    }
    else
      return next(null);
  });
}

function getDownloadLink(tmvdbID, options, next) {

  console.log("begin getDownloadPage http://hdpopcorns.in/select-movie-quality.php");
  
  return request.post({ url: "http://hdpopcorns.in/select-movie-quality.php", form: options }, function (error, response, html) {
    if (!error && response.statusCode == 200) {
      var $ = cheerio.load(html);

      var now = new Date();
      var version = dateFormat(now, "yyyymmdd");

      var btn_720p = $('div.thecontent').children('#btn_720p').children('a').attr('href');
      var btn_1080p = $('div.thecontent').children('#btn_1080p').children('a').attr('href')

      var extension = '.mp4';
      if(btn_720p.indexOf('.mkv') !== -1)
        extension  = '.mkv';
      else if(btn_720p.indexOf('.avi') !== -1)
        extension  = '.avi';
      
      if (btn_720p.indexOf("http:///downloads/") === -1)
        return next(btn_720p,tmvdbID + " " + version + "v1",extension);
      else if (btn_1080p.indexOf("http:///downloads/") === -1)
        return next(btn_1080p,tmvdbID + " " + version + "v1",extension);
      else
        return next(null);
    }
    else
      return next(null);
  });
}




function addToIDM(link,name,extension)
{
  const { execFile } = require('child_process');
  var idmPath = 'C:/Program Files (x86)/Internet Download Manager/IDMan.exe';
  var descPath = 'C:/phimLinh'

  execFile(idmPath,['/s', '/a', '/d', link, '/p' , descPath , '/f' , name + extension],function(err,data) {  
    console.log("Add to IDM Done");                       
  });  
}


