
var request = require('request');
var cheerio = require('cheerio')
var http = require('http');
var https = require('https');
var urlEndocde = require('url');
var dateFormat = require('dateformat');
var ggsheet = require('./GGSheet');
const fs = require('fs');
const readline = require('readline');
const google = require('googleapis');
var downloader = require('./downloader');

//addToIDM("http://85.25.211.138/downloads/HDPOPCORNS.Blade-Runner-2049-2017-720p.mp4?st=Av71UTtiFf4Lk9n8NAOubA&e=1515587327","1231 20180108v1");
ggsheet.repareAuthorize(listMajors);
// parse(0,50620,"The Twilight Saga: Breaking Dawn - Part 2",function(index,link,name,extension)
// {
//   console.log("link = ",link);
// });

function parse(index,tmvdbID, movieName, next) {
  // var url = "http://hdpopcorns.in/?s=" + movieName;
  // console.log("parse" + url);
  var strurl = "http://hdpopcorns.in/?s=" + movieName;
  //var strurl = "https://oload.win/stream/d_vsiBSaUPM~1524240149~27.78.0.0~5ZUGLQqj";
  console.log("parse" + strurl);
    var options = {
      url: '' + strurl,
      timeout: 10000
  }
  return request(options, function (error, response, html) {
    console.log("request error" + error);
    console.log("request response" + response);

    if (!error && response.statusCode == 200) {
      console.log("request 000000000");
      var $ = cheerio.load(html);
      console.log("request 000000001");
      var demoLink = $('article.latestPost.excerpt.first').children('a').attr('href');
      console.log("request 000000002");
      var name = $('article.latestPost.excerpt.first').children('a').attr('title');
      console.log("request 000000003" + name);
     
      
      if (demoLink && name)
      {
        var name1 = name.replace("’","'").replace("–","-").toLocaleLowerCase();
        var name2 = movieName.replace("’","'").replace("–","-").toLocaleLowerCase();
        if(name1.indexOf(name2) !== -1)
        {
          return getDownloadPage(index,tmvdbID, demoLink, next);
        }
      }
      
      return next(null);
    }
    else
      return next(null);
  });
}


function listMajors(auth) {
  var sheets = google.sheets('v4');

  fs.readFile('./autodownload/save.json', (err, data) => {  

    if (err) throw err;
    let saveJson = JSON.parse(data);
    console.log(saveJson);
    
    var startIdx = saveJson.currentID;
    var lastIndex = startIdx;
    var readCount = saveJson.readCount;
    var lastestVersion = saveJson.lastestVersion;
    var now = new Date();
    var version = dateFormat(now, "yyyymmdd") + "v1";

    var currentCount  = 0;

    if(version === lastestVersion)
    {
      console.log("to day you had been import " + readCount + " for version = " + lastestVersion);
      return;
    }
    var range = 'Sheet49!A'+startIdx+':C' + (startIdx + 2000); //maxium 200
    console.log("range = " + range);

    sheets.spreadsheets.values.get({
      auth  : auth,
      spreadsheetId: '19G_UTLVOO1Ils-I809VMLjVyUCjlmBjPpOHygIQfs9I',
      range: range,
    }, function (err, response) {
      if (err) {
        console.log('The API returned an error: ' + err);
        return;
      }
      var data = response.values;
      if (data.length == 0) {
        console.log('No data found.');
      } else {

        var fs = require('fs');
        var logStream = fs.createWriteStream('log.txt', { 'flags': 'w' });
        // use {'flags': 'a'} to append and {'flags': 'w'} to erase and write a new file
        var temp = 0;
        var header = data[0];

        var listMovies = [];
        for (var i = 1; i < data.length; i++) {
          var row = data[i];
          var tmvdbID = parseInt(row[0]);
          var name = row[1] + " " + row[2].substring(0, 4);

          console.log("tmvdbID = " + tmvdbID);
          console.log("name = " + name);
          console.log("i = " + i);
          listMovies.push({ index : i , tmvdbID: tmvdbID, name: name });
        }

        function* genNextMovie() {
          for (var i = 0; i < listMovies.length; i++) {
            var element = listMovies[i];
            yield element;
          }
          return null;
        }

        var logStream = fs.createWriteStream('downloadLink.txt', { 'flags': 'w' });

        var movielinklist = genNextMovie();
        var movieLink = movielinklist.next().value;
        parse(movieLink.index,movieLink.tmvdbID, movieLink.name, saveDownloadLinkToFile);
        function saveDownloadLinkToFile(index,link,name,extension) {
          if(link)
          {
            console.log('get index = ' + (index + startIdx));
            console.log('get link = ' + link);
            console.log('get name = ' + name);
            console.log('get extension = ' + extension);
            logStream.write(link + "\n");

            var savejson = {
              "currentID" : (index + startIdx - 1) ,
              "lastIndex" : lastIndex,
              "readCount" : readCount,
              "lastestVersion" : name.split(" ")[1]
            }
          
            let data = JSON.stringify(savejson);  
            fs.writeFileSync('./autodownload/save.json', data);  

            currentCount++;
            addToIDM(link,name,extension);
          }

          if(currentCount >= readCount)
          {
            console.log('Import Done!');
            return null;
          }

          var nextLink = movielinklist.next().value;
          if(nextLink)
            return parse(nextLink.index,nextLink.tmvdbID, nextLink.name, saveDownloadLinkToFile);
          else
          {
            console.log('Import Done!');
            return null;
          }
        }
        
      }
    });
  });

}



function getDownloadPage(index,tmvdbID, url, next) {
  return request(url, function (error, response, html) {
    if (!error && response.statusCode == 200) {
      var $ = cheerio.load(html);
      var form = $('form').filter(function (i, el) {
        return $(this).attr('action') === '/select-movie-quality.php';
      });
      var inputFields = form.children('input');
      var options = {};
      for (var i = 0; i < inputFields.length; i++) {
        var key = inputFields[i].attribs.name;
        var value = inputFields[i].attribs.value;
        options[key] = value;
      }
      return getDownloadLink(index,tmvdbID, options, next);
    }
    else
      return next(null);
  });
}

var logdownloadmovies = fs.createWriteStream('downloadmovieID.txt', { 'flags': 'w' });

function getDownloadLink(index,tmvdbID, options, next) {
  return request.post({ url: "http://hdpopcorns.in/select-movie-quality.php", form: options }, function (error, response, html) {
    if (!error && response.statusCode == 200) {
      var $ = cheerio.load(html);

      var now = new Date();
      var version = dateFormat(now, "yyyymmdd");

      var btn_720p = $('div.thecontent').children('#btn_720p').children('a').attr('href');
      var btn_1080p = $('div.thecontent').children('#btn_1080p').children('a').attr('href')

      var extension = '.mp4';
      if(btn_720p.indexOf('.mkv') !== -1)
        extension  = '.mkv';
      else if(btn_720p.indexOf('.avi') !== -1)
        extension  = '.avi';
      
      if (btn_720p.indexOf("http:///downloads/") === -1)
      {
        logdownloadmovies.write(tmvdbID + "\n");
        return next(index,btn_720p,tmvdbID + " " + version + "v1",extension);
      }
      else if (btn_1080p.indexOf("http:///downloads/") === -1)
      {
        logdownloadmovies.write(tmvdbID + "\n");
        return next(index,btn_1080p,tmvdbID + " " + version + "v1",extension);
      }
      else
        return next(null);
    }
    else
      return next(null);
  });
}




function addToIDM(link,name,extension)
{
  const { execFile } = require('child_process');
  var idmPath = 'C:/Program Files (x86)/Internet Download Manager/IDMan.exe';
  var descPath = 'C:/upphim'

  execFile(idmPath,['/s', '/a', '/d', link, '/p' , descPath , '/f' , name + extension],function(err,data) {  
    console.log("Add to IDM Done");                       
  });  
}


