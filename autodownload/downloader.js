var os   = require('os');
var path = require('path');
var Downloader = require('mt-files-downloader');

var download = function(url,savePath,next)
{
    var downloader = new Downloader();
    console.log('File will be downloaded from '+ url +' to '+ savePath);
    var dl = downloader.download(url, savePath)
            .start();
    require('./_handleEvents')(dl);
    require('./_printStats')(dl);

    dl.on('start', function(dl) { 
        console.log("begin download");
     });
    dl.on('error', function(dl) { 
        //dl.destroy();
        next(-1);
     });
    dl.on('end', function(dl) { 
        //dl.destroy();
        next(3);
     });
    dl.on('stopped', function(dl) { 
        //dl.destroy();
        next(-2);
     });
    dl.on('destroyed', function(dl) {
        //dl.destroy();
        next(-3);
     });
    dl.on('retry', function(dl) { 
        //dl.destroy();
        next(2);
     });
}

exports.default = { download: download };
module.exports = exports['default'];