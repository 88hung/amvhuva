var geoip = require('geoip-lite');
var request = require('request');

var options = {
    url: 'http://104.154.199.96:3001/api/admin/getCurrentIpList',
    headers: {
      'pattern': 'IPC_*'
    }
  };

request.post(options, function (error, response, body) {
    var data = JSON.parse(body);
    data.forEach(element => {
        var geo = geoip.lookup(geoip.pretty(element.ip.replace("IPC_", "")));
        if(geo && geo.country == 'VN')
        {
            console.log("ip = " + element.ip + " count = " + element.count + " city = " + geo.city);
        }
    });
});
//console.log(geo);