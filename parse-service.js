const express = require('express')
const app = express()


var request = require('request');
var cheerio = require('cheerio')

function parse(req, res,next) {
    var gglink = req.query.gglink;
    var code = "79468658" || req.query.code;

    request(gglink, function (error, response, html) {
      if (!error && response.statusCode == 200) {
        var $ = cheerio.load(html);
  
        var list_AF_initDataCallback = $('body > script').map(function (i, x) {
          return x.children[0];
        }).filter(function (i, x) {
          return x && x.data.match(/AF_initDataCallback/);
        }).toArray();
  
        list_AF_initDataCallback.forEach(function (element) {
          if (element) {
            var scriptText = element.data.replace(/\n/g, "");
            if (scriptText.search(code) > -1) {
              if (scriptText.search(code) > -1) {
                var listLinkStr = /return(.*]}])/.exec(scriptText);
                if(listLinkStr)
                {
                  var jsonString = listLinkStr[0].replace("return ", "");
                  var sources = JSON.parse(jsonString);
                  var data = sources[sources.length - 1];
                  if(data && data[code] && data[code].length > 0){
                    console.log("===========" + gglink);
                    return res.json(encodeStream(data[code][0][0][2]));
                  }
                  else
                  {
                    console.log("===============================================link not ready====================================");
                  console.log("===========" + gglink);
  
                  return res.status(300).send([]);
                  }
                }
                else
                {
                  console.log("===============================================link not ready====================================");
                  console.log("===========" + gglink);
  
                  return res.status(300).send([]);
                }
              }
            }
          }
        }, this);
      } else return res.status(500).send([]);
    });  
}

function encodeStream(str) {
  var list = [];
  var streams = str.split("url=");
  streams.forEach(function(element) {
    if(element != "")
    {
      //console.log("parse =" + element);
      var params = getQueryParams(element);
      list.push(params);
    }
  }, this);

  return list;
}

function getQueryParams(qs) {
  var splits = decodeURIComponent(qs).split("&");
  var url = decodeURIComponent(splits[0]);
  var itag = decodeURIComponent(splits[1]).split("=")[1];
  var type = decodeURIComponent(splits[2]).split("=")[1];
  var quality = decodeURIComponent(splits[3]).split("=")[1];

  return {url : url, itag : itag ,type : type, quality : quality};
  
}

function checkLinkDie(req,res,next)
{
  //var link = req.url.split("link=")[1];
  var link  = "https://video.xx.fbcdn.net/v/t42.9040-2/10000000_634881590202557_8641631369585229824_n.mp4?_nc_cat=0&efg=eyJybHIiOjE1MDAsInJsYSI6MTAyNCwidmVuY29kZV90YWciOiJzdmVfaGQifQ%3D%3D&rl=1500&vabr=964&oh=66a2bdb2ad7fc917f3be0a122ccfed35&oe=5B46396E"
  console.log("check die link = " + link)
  if(link)
  {
  // link = "https://stackoverflow.com/questions/11371310/node-js-saving-a-get-requests-html-response";
    var method = "HEAD";
    if(link.indexOf("video.xx.fbcdn") !== -1) {
      method = "GET"
    }

    console.log("method = " + method)
    return request({ url: link, followRedirect: false,method: method})
      .on('response', function(response) {
          console.log(response.statusCode) // <--- Here 200
          return res.json({code : response.statusCode})
      })
      .on("error", function(err){
          console.log("Problem reaching URL: ", err);
          //return res.json({code : -1})
      })
      .on("finish", function(response) {
          console.log("done");
          //return res.json({code : -1})
      })
  }
  else 
  {
    return res.json({code : -1})
  }
}

app.get('/api/ggparse', (req, res) => {
    return parse(req,res,null);
})

app.get('/api/checkdie', (req, res) => {
  return checkLinkDie(req,res,null);
})


app.listen(3002, () => console.log('Example app listening on port 3002!'))
