//initialise the export results collection
db.export.movie.drop();
//create a cursor containing the contents of the list1 collection
var cursor = db.movies.find().sort({"popularity":-1})

while (cursor.hasNext()) {
    var doc = cursor.next();
    var doc_export = {};
    doc_export.tmvdbID = doc.tmvdbID;
    doc_export.ggLinks = doc.ggLinks;
    doc_export.name = doc.title;
    doc_export.release_date = doc.release_date;
    doc_export.popularity = doc.popularity;
    if(doc.subtitle_identify)
        doc_export.subtitle_identify = doc.subtitle_identify;
    else
        doc_export.subtitle_identify = "";
    
    if(doc_export.tmvdbID != 0 && doc_export.subtitle_identify.length == 0)
        db.export.movie.insert(doc_export)
}
print(db.export.movie.count() + " matching documents found");
//mongo "localhost:27017/amvhuva" exportmovies_all.js
//mongoexport --host localhost --db amvhuva --collection export.movie --type=csv --out movie.csv --fields="tmvdbID,name,release_date,popularity,subtitle_identify"
