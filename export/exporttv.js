//initialise the export results collection
db.export.tv.drop();
//create a cursor containing the contents of the list1 collection
var cursor = db.movies.find({ 'tv.id': { $ne: null } })
//.sort({"popularity":-1})

while (cursor.hasNext()) {
    var doc = cursor.next();
    var tv = doc.tv;
    
    tv.sessions.forEach(function(element) {
        var  cursor_session = db.sessions.find({id:element})
        while (cursor_session.hasNext()) {
            var session = cursor_session.next();
            for(var i = 0; i < session.episode_count ; i++)
            {
                var doc_export = {};
                doc_export.tvid = tv.id;
                doc_export.name = doc.title;
                doc_export.sessionID = session.id;
                doc_export.date = session.air_date;
                doc_export.sessionName = "session" + session.season_number;
                doc_export.episode = i;
                db.export.tv.insert(doc_export)
            }
        }
        
    }, this);
    //db.export.tv.insert(doc);
    // //Check if the document exists in the list2 collection
    // list2 = db.list2.find({"<id_fieldname>": doc.<id_fieldname>});
    // if (list2.hasNext()) {
    //     //if it does exist, add the document from list1 to the new export collection
    //     db.export.results.insert(doc);
    // }
}
print(db.export.tv.count() + " matching documents found");

//mongo "localhost:27017/amvhuva" exporttv.js

//mongoexport --host localhost --db amvhuva --collection export.tv --type=csv --out tv.csv --fields="tvid,name,sessionID,date,sessionName,episode"