//initialise the export results collection
db.export.movie.requestCount.drop();
//create a cursor containing the contents of the list1 collection
var cursor = db.movies.find().limit(count).sort({"requestCount":-1})

while (cursor.hasNext()) {
    var doc = cursor.next();
    var doc_export = {};
    doc_export.tmvdbID = doc.tmvdbID;
    doc_export.ggLinks = doc.ggLinks;
    doc_export.name = doc.title;
    doc_export.release_date = doc.release_date;
    doc_export.requestCount = doc.requestCount || 0;
    doc_export.viewCount = doc.viewCount || 0;
    doc_export.ggLinksLeng = doc.ggLinks.length || 0;
    db.export.movie.requestCount.insert(doc_export)
}
print(db.export.movie.requestCount.count() + " matching documents found");
//mongo "localhost:27017/amvhuva" exportmovies_requestCount.js

//mongoexport --host localhost --db amvhuva --collection export.movie.requestCount --type=csv --out movie_requestCount.csv --fields="tmvdbID,name,release_date,requestCount"