
const express = require('express')
const app = express()


var request = require('request');
var cheerio = require('cheerio')

parse3("https://photos.google.com/share/AF1QipN-wkAq03BT67OVDcYimUYToT0kGe0h6L2of09ZkIV7kbariFSNJCXCXiJqg_05-w/photo/AF1QipPtOEzHhboajyNZXxoHyp7X_-0PHiiN6X1Amp3f?key=eldRLWFRZUdET3F2R2NpYzAwc1NBUWc5aDlKR01B",function(links){
  console.log("----" + links);
})

function parse3(link,next) {
  var gglink = link;
  var code = "79468658" || req.body.code;

  request(gglink, function (error, response, html) {
    if (!error && response.statusCode == 200) {
      var $ = cheerio.load(html);

      var list_AF_initDataCallback = $('body > script').map(function (i, x) {
        return x.children[0];
      }).filter(function (i, x) {
        return x && x.data.match(/AF_initDataCallback/);
      }).toArray();

      list_AF_initDataCallback.forEach(function (element) {
        if (element) {
          var scriptText = element.data.replace(/\n/g, "");
          if (scriptText.search(code) > -1) {
            var jsonString = /return(.*]]}])/.exec(scriptText)[0].replace("return ", "");
            var sources = JSON.parse(jsonString);
            var data = sources[sources.length - 1];
            return next(encodeStream(data[code][0][0][2]));
          }
  }
      }, this);
    } else return next(null)
  });  
}

function encodeStream(str) {
  var list = [];
  var streams = str.split("url=");
  streams.forEach(function(element) {
    if(element != "")
    {
      //console.log("parse =" + element);
      var params = getQueryParams(element);
      list.push(params);
    }
  }, this);

  return list;
}

function getQueryParams(qs) {
  var splits = decodeURIComponent(qs).split("&");
  var url = decodeURIComponent(splits[0]);
  var itag = decodeURIComponent(splits[1]).split("=")[1];
  var type = decodeURIComponent(splits[2]).split("=")[1];
  var quality = decodeURIComponent(splits[3]).split("=")[1];

  return {url : url, itag : itag ,type : type, quality : quality};
  
}
