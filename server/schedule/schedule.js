const schedule = require('node-schedule');
const https = require('https');

import Movie from '../models/movie.model';
import AdminCtrl from '../controllers/movie.admin.controller';
import CongifCtrl from '../controllers/config.controller';


function reportToSlack(text) {
    const url = "https://slack.com/api/chat.postMessage?token=xoxp-242080693313-261025019845-267157917989-839784e5b2fcf9dd569db1fc34a23211&channel=CA3N0NK2A&text=" + text;
    https.get(url, (resp) => {
        let data = '';
        resp.on('data', (chunk) => {
            data += chunk;
        });
        resp.on('end', () => {
            console.log("Sent to slack link " + text);
        });

    }).on("error", (err) => {
        console.log("Error: " + err.message);
    });
}



function resetCountPerDay() {
    console.log('SCHEDULE ------------ start resetCountPerDay');
    var job = schedule.scheduleJob({ hour: 0, minute: 0, dayOfWeek: new schedule.Range(0, 6) }, function () {
        console.log('SCHEDULE ------------ RESET COUNT PER DAY');
        CongifCtrl.deleteKey("PUSH_*");
        Movie.resetCountPerDaylist(3000);
    });

    var job2 = schedule.scheduleJob({ hour: 10, minute: 0, dayOfWeek: new schedule.Range(0, 6) }, function () {
        console.log('SCHEDULE ------------ UPDATE TV');
        AdminCtrl.importTVSchedule();
    });

    setInterval(function () {
        const used = (process.memoryUsage().heapUsed + process.memoryUsage().heapTotal) / 1024 / 1024;
        console.log(`The script uses approximately ${Math.round(used * 100) / 100} MB`);
        AdminCtrl.updateMovieNotReady2();

        if (global.gc) {
            global.gc();
            reportToSlack(`global_gc_currentMem_+${Math.round(used * 100) / 100}`);
        } else {
            reportToSlack('No GC hook! Start your program as `node --expose-gc file.js`.');
        }

    }, 1000 * 60 * 15)
}

function backupDB() {
    console.log('SCHEDULE ------------ start backupDB');
    var job = schedule.scheduleJob({ hour: 12, minute: 0, dayOfWeek: new schedule.Range(0, 6) }, function () {
        const { spawn } = require('child_process'),
            mongodump = spawn('mongodump', ['--out', __dirname + '/../../export/backup/']);

        mongodump.stdout.on('data', data => {
            console.log(`stdout: ${data}`);
        });

        mongodump.stderr.on('data', data => {
            console.log(`stderr: ${data}`);
        });

        mongodump.on('close', code => {
            console.log(`child process exited with code ${code}`);
        });
    });
}

function start() {
    resetCountPerDay();
    backupDB();
}
export default { start };