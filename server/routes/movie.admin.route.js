import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import AdminCtrl from '../controllers/movie.admin.controller';
import ggCtrl from '../controllers/googlePhotoParser.controller';
import ConfigCtrl from '../controllers/config.controller';
import IPFillter from '../controllers/ipfillter';

const router = express.Router();

router.route('/search')
.get(AdminCtrl.search);

router.route('/importDatabase')
.post(AdminCtrl.importDataBase);

router.route('/importTVDatabase')
.post(AdminCtrl.importTVDatabase)

router.route('/importVideos')
.post(AdminCtrl.importVideos);

router.route('/importGenre')
.post(AdminCtrl.importGenre);

router.route('/importTVGenre')
.post(AdminCtrl.importTVGenre);

router.route('/importReview')
.post(AdminCtrl.importReview);

router.route('/importFromGGSheet')
.get(AdminCtrl.importFromGGSheet)

router.route('/exportMovieCSV')
.get(AdminCtrl.exportMovieCSV)

router.route('/exportMovieCSVRequestCount')
.get(AdminCtrl.exportMovieCSVRequestCount)

router.route('/exportNoSubtitleCSV')
.get(AdminCtrl.exportNoSubtitleCSV)

router.route('/updateGGLink')
.post(AdminCtrl.updateOrCreateGGLink);

router.route('/updateSubtitle')
.post(AdminCtrl.updateSubtile)

router.route('/deleteLink')
.post(AdminCtrl.deletelink)

router.route('/deletelinkByID')
.post(AdminCtrl.deleteLinkByID)

router.route('/createOrUpdateConfig')
.post(ConfigCtrl.createOrUpdateConfig);

router.route('/getallconfigs')
.get(ConfigCtrl.getallconfigs);

router.route('/updateTV')
.post(AdminCtrl.updateTV)

router.route('/importTrailer')
.post(AdminCtrl.importTrailer);

router.route('/importVideoTrailer')
.post(AdminCtrl.importVideoTrailer);

router.route('/resetViewPerDay')
.post(AdminCtrl.resetViewPerDay);

router.route('/resetRequestCount')
.post(AdminCtrl.resetRequestCount);

router.route('/getHeapDump')
.post(AdminCtrl.getHeapDump);


router.route('/getCurrentIpList')
.post(IPFillter.current_list);

router.route('/getReportIpList')
.post(IPFillter.report_list);

router.route('/resetRedis')
.post(IPFillter.clear);

router.route('/updateAdult')
.post(AdminCtrl.updateAdult);



router.route('/updateDriveLinks')
.post(AdminCtrl.updateDriveLinks);

router.route('/removeDriveLink')
.post(AdminCtrl.removeDriveLink);

router.route('/GetallLink429')
.post(AdminCtrl.getAllLink429);

router.route('/updateMovieNotReady')
.post(AdminCtrl.updateMovieNotReady)

router.route('/updateLimitPush')
.post(ConfigCtrl.updateLimitPush)

export default router;
