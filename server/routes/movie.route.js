import express from 'express';
import movieCtrl from '../controllers/movie.controller';
import ggCtrl from '../controllers/googlePhotoParser.controller';
import ConfigCtrl from '../controllers/config.controller';
import TokenMgr from '../controllers/token.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/genre/movie/list')
    .get(movieCtrl.getGenreList)

router.route('/genre/tv/list')
    .get(movieCtrl.getTVGenreList)

router.route('/discover/series')
    .get(movieCtrl.getSeries)

router.route('/discover/session')
    .get(movieCtrl.getSession)

router.route('/discover/movieDetail')
    .get(movieCtrl.movieDetail)

router.route('/discover/movie')
    .get(movieCtrl.list)

router.route('/discover/tv')
    .get(movieCtrl.listtv)

router.route('/discover/movie_by_genres')
    .get(movieCtrl.listbyGenre);

router.route('/discover/tv_by_genres')
    .get(movieCtrl.listTVbyGenre);

router.route('/movie/search')
    .get(movieCtrl.search);

router.route('/movie/:id/videos')
    .get(TokenMgr.check_api_key, movieCtrl.getVideos);

router.route('/incsViewLink')
    .get(movieCtrl.increaGGLinkViewCount);

router.route('/onNewLink')
    .get(movieCtrl.onNewLink);

router.route('/getListClientLink')
    .get(movieCtrl.getListClientLink);


router.route('/incsViewMovie')
    .get(movieCtrl.increaViewCount);

router.route('/request')
    .get(movieCtrl.increaRequest);

router.route('/movie/:id/reviews')
    .get(movieCtrl.getReview);

router.route('/report')
    .get(movieCtrl.reportLink);

router.route('/notification/video/error/:videoID')
    .get(movieCtrl.notifyLinkdie);

router.route('/mobile-config')
    .get(ConfigCtrl.getConfig);

router.route('/reportViewCount/:id')
    .get(movieCtrl.reportViewCount)

////////////////////////////////////DEBUG///////////////////////////////////
router.route('/getallvideo')
    .get(movieCtrl.getAllVideo);

router.route('/getStreams')
    .get(movieCtrl.getStreams);

router.route('/ggparse')
    .get(ggCtrl.parse);

router.route('/getLinkList')
    .get(movieCtrl.getLinkList);

router.route('/getAllReviews')
    .get(movieCtrl.getAllReviews);

router.route('/getLinkCount')
    .get(movieCtrl.getLinkCount);

router.route('/getViewPerDay')
    .get(movieCtrl.getViewPerDay);


///////////////////////////////////~DEBUG/////////////////////////////////////////




export default router;
