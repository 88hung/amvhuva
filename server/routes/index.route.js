import express from 'express';
import userRoutes from './user.route';
import authRoutes from './auth.route';
import movieRoutes from './movie.route';
import movieAdminRoutes from './movie.admin.route';
import TokenMgr from '../controllers/token.controller';
const router = express.Router(); // eslint-disable-line new-cap

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK')
);
router.use('/robots.txt', (req, res) => {
    res.setHeader('Content-Type', 'text/plain');
    res.end('User-agent: *\nDisallow: /');
  }
);
router.use('/mobile-configparse_config_fail', (req, res) => {
  res.setHeader('Content-Type', 'text/plain');
  res.end('User-agent: *\nDisallow: /');
}
);
// mount user routes at /users
router.use('/users', userRoutes);

// mount auth routes at /auth
router.use('/auth', authRoutes);

// mount moive routes at /movie
router.use('/', movieRoutes);

// mount movieadmin routes at /movie.admin
router.use('/admin', movieAdminRoutes);


export default router;
