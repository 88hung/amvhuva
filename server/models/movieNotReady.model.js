
import mongoose from 'mongoose';

const ObjectId = mongoose.Schema.Types.ObjectId;

const MovieNotReady = mongoose.Schema({
    movieID: {
        type: String,
        default: ""
    },
    GGLink: {
        type: String,
        default: ""
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    parseCount: {
        type: Number,
        default: 0
    }
});

MovieNotReady.statics = {
    /**
     * Get user
     * @param {ObjectId} id - The objectId of user.
     * @returns {Promise<User, APIError>}
     */
    get(movieID) {
        return this.find({ "movieID": movieID })
            .exec()
            .then((movies) => {
                return movies;
            });
    },

    list() {
        return this.find({})
            .exec();
    },
    createOrUpdate(movieID, GGLink, next) {
        var query = { movieID: movieID, GGLink: GGLink },
            update = { $inc: { parseCount: 1 } },
            options = { upsert: true, new: true, setDefaultsOnInsert: true };
        return this.findOneAndUpdate(query, update, options)
            .exec()
            .then(result => next(result))
    },

    incsParseCount(movieID, GGLink, next) {
        var query = { movieID: movieID, GGLink: GGLink },
            update = { $inc: { parseCount: 1 } };
        return this.findOneAndUpdate(query, update)
            .exec()
            .then(result => next(result))
            .catch(e => next(null));
    },
};

export default mongoose.model('MovieNotReady', MovieNotReady);
