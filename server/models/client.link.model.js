
import mongoose from 'mongoose';

const ObjectId = mongoose.Schema.Types.ObjectId;

var cachegoose = require('cachegoose');
cachegoose(mongoose, {
    port: 27017,         /* the query results will be cached in memory. */
    host: 'localhost'
});
const CACHE_TIME = 60;

const ClientLinkSchema = mongoose.Schema({
  movieID: {
    type: Number,
    default: 0
  },
  link: {
    type: String,
    default: ""
  },
  needToResolver: {
    type: Boolean,
    default: false
  },
  host: {
    type: String,
    default: ""
  },
  createAt: {
    type: Date,
    default: Date.now
  }
});

ClientLinkSchema.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .cache(CACHE_TIME)
      .exec()
      .then((link) => {
        if (link) {
          return link;
        }
        return null;
      })
      .catch(e => null)
  },
  getLinkByMovieID(movieID, needToResolver) {
    return this.find({ movieID: movieID, needToResolver: needToResolver })
      .cache(CACHE_TIME)
      .exec()
      .then((links) => {
        if (links) {
          return links;
        }
        return null;
      })
      .catch(e => { return null })
  },
  getAllLinkByMovieID(movieID) {
    return this.find({ movieID: movieID })
      .cache(CACHE_TIME)
      .exec()
      .then((links) => {
        if (links) {
          return links;
        }
        return null;
      })
      .catch(e => { return null })
  },

  createOrUpdate(movieID, link, host, needToResolver) {

    console.log("CLIENT LINK createOrUpdate movieID =" + movieID);
    console.log("CLIENT LINK createOrUpdate link =" + link);
    console.log("CLIENT LINK createOrUpdate host =" + host);
    console.log("CLIENT LINK createOrUpdate needToResolver =" + needToResolver);

    return this.find({ movieID: movieID, needToResolver: needToResolver })
      .exec()
      .then(results => {
        if (results) {
          while (results.length > 5) {
            console.log("BEGIN REMOVE LINK =" + results.length);
            var clientLink = null;
            results.forEach(element => {
              if (clientLink == null)
                clientLink = element;
              else {
                if (clientLink.createAt <= element.createAt)
                  clientLink = element;
              }
            });

            clientLink.remove();

            var index = results.indexOf(clientLink);
            if (index > -1) {
              results.splice(index, 1);
            }
            else {
              console.log("REMOVE OLD LINK ITEM NOT FOUND");
              return { "message": "done" };
            }
            console.log("REMOVE OLD LINK =" + clientLink.link);
          }
        }

        var query = { movieID: movieID, link: link },
          update = { movieID: movieID, link: link, host: host, needToResolver: needToResolver },
          options = { upsert: true, new: true, setDefaultsOnInsert: true };
        return this.findOneAndUpdate(query, update, options)
          .exec()
          .then(result => result)

      })



  },

  removebyID(id) {
    return this.remove({ "_id": id })
      .exec()
      .then(abc => abc)
      .catch(e => e)
  },
  getlist() {
    return this.find()
      .then(list => list)
      .catch(e => null)
  },
  getlistDontNeedToResolver() {
    return this.find({ needToResolver: false })
      .then(list => list)
      .catch(e => null)
  },
  getpriorityLink(link) {
    if (link.indexOf("rapid"))
      return 10;
    if (link.indexOf("openload"))
      return 9;
    if (link.indexOf("ok.ru"))
      return 8;
    if (link.indexOf("vstream"))
      return 7;
    return 6;
  }
};

export default mongoose.model('ClientLink', ClientLinkSchema);
