
const mongoose = require('mongoose')
const AutoIncrement = require('mongoose-sequence')(mongoose);
const ObjectId = mongoose.Schema.Types.ObjectId;

var cachegoose = require('cachegoose');

cachegoose(mongoose, {
    port: 27017,         /* the query results will be cached in memory. */
    host: 'localhost'
});

const CACHE_TIME = 60;

// tạo cấu trúc db
const MovieSchema = mongoose.Schema({
    vote_count: {
        type: Number,
        default: 0
    },
    tmvdbID: {
        type: Number,
        default: 0
    },
    video:
    {
        type: Boolean,
        default: false
    },
    tv: {
        id: {
            type: Number
        },
        sessions: {
            type: Array
        },
        isActive: {
            type: Boolean,
            default: false
        }
    },
    vote_average: {
        type: Number,
        default: 0
    },
    title: {
        type: String,
        default: 'Passage of Venus'
    },
    popularity: {
        type: Number,
        default: 0.0
    },
    poster_path:
    {
        type: String,
        default: '/9jtgJphKW836gISyzdUKWnZ6yrB.jpg'
    },
    original_language: {
        type: String,
        default: 'en'
    },
    original_title:
    {
        type: String,
        default: 'Passage of Venus'
    },
    genre_ids:
    {
        type: Array,
        default: []
    },
    backdrop_path:
    {
        type: String,
        default: '/9jtgJphKW836gISyzdUKWnZ6yrB.jpg'
    },
    adult: {
        type: Boolean,
        default: false
    },
    overview:
    {
        type: String,
        default: 'Photo sequence of the rare transit of Venus over the face of the Sun, one of the first chronophotographic sequences.'
    },
    release_date:
    {
        type: Date,
        default: Date.now
    },
    createdAt: {
        type: Date,
        default: new Date(1970, 11, 24, 10, 33, 30, 0)
    },
    quality: {
        type: String,
        default: "HD"
    },
    ggLinks: {
        type: [ObjectId],
        default: []
    },
    ggdriveLinks: {
        type: [String],
        default: []
    },
    status: {
        type: Number,
        default: 0// inactive 1 active 2 die 
    },
    subtitle_identify: {
        type: String,
        default: ""
    },
    viewCount: {
        type: Number,
        default: 0
    },
    requestCount: {
        type: Number,
        default: 0
    },
    countPerDay: {
        type: Number,
        default: 0
    }
}, { _v: false, usePushEach: true });

MovieSchema.plugin(AutoIncrement, { inc_field: 'id' });
MovieSchema.index({ title: 'text', original_title: 'text' });

MovieSchema.statics = {
    /**
     * Get user
     * @param {ObjectId} id - The objectId of user.
     * @returns {Promise<User, APIError>}
     */
    get(id, next) {
        return this.findOne({ id: id })
            .cache(CACHE_TIME)
            .exec()
            .then((movie) => {
                if (movie) {
                    return next(movie);
                }
                return next(null)
            });
    },

    getbyTMVDB(tmvdbID, next) {
        return this.findOne({ tmvdbID: tmvdbID })
            .exec()
            .then((movie) => {
                return next(movie)
            });
    },
    getbyTV(tvID, next) {
        return this.findOne({ 'tv.id': tvID })
            .exec()
            .then((movie) => {
                if (movie) {
                    return next(movie);
                }
                return next(null)
            });
    },
    setFromTMVDB(newMovie) {
        return this.findOne({ tmvdbID: newMovie.id })
            .exec()
            .then((movie) => {
                if (movie) {
                    movie.popularity = newMovie.popularity;
                    movie.vote_average = newMovie.vote_average;
                    movie.release_date = newMovie.release_date;
                    movie.overview = newMovie.overview;
                    movie.adult = newMovie.adult;
                    movie.poster_path = newMovie.poster_path;
                    movie.backdrop_path = newMovie.backdrop_path;
                    movie.save();
                }
                else {
                    var Movie = mongoose.model('Movie', MovieSchema);
                    var movieSave = new Movie(newMovie);
                    movieSave.save();
                }
            });
    },
    setFromTMVDBTV(newMovie, next) {
        return this.findOne({ 'tv.id': newMovie.tv.id })
            .exec()
            .then((movie) => {
                if (movie) {
                    movie.doc = newMovie;
                    return next(movie);
                }
                else {
                    var Movie = mongoose.model('Movie', MovieSchema);
                    var movieSave = new Movie(newMovie);
                    return next(movieSave);
                }
            });
    },
    setIsSeries(movieID) {
        return this.findOne()
    },
    /**
     * List users in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of users to be skipped.
     * @param {number} limit - Limit number of users to be returned.
     * @returns {Promise<User[]>}
     */

    list({ skip = 0, limit = 20, sort = "popularity" } = {}) {
        var sortobj = {};
        sortobj[sort] = -1;
        var todayDate = new Date(), weekDate = new Date();
        weekDate.setTime(todayDate.getTime() - (28 * 24 * 3600000));

        return this.find({ 'tv.id': null })
            .cache(CACHE_TIME)
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec();
    },

    listNew({ skip = 0, limit = 20, sort = "createdAt" } = {}) {
        var sortobj = {};
        sortobj[sort] = -1;
        var todayDate = new Date(), weekDate = new Date();
        weekDate.setTime(todayDate.getTime() - (28 * 24 * 3600000));
        return this.find({ 'release_date': { $lt: weekDate }, 'tv.id': null })
            .cache(CACHE_TIME)
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec();
    },

    listMovieOnly({ skip = 0, limit = 20, sort = "popularity" } = {}) {
        var sortobj = {};
        sortobj[sort] = -1;
        return this.find({ 'tv.id': null })
            .cache(CACHE_TIME)
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec();
    },


    listHasStreams({ skip = 0, limit = 20, sort = "popularity" } = {}) {
        var sortobj = {};
        sortobj[sort] = -1;
        return this.find(
            {
                $and: [
                    { $or: [{ 'tv.isActive': true }, { ggLinks: { $gt: [] } }] },
                    { $or: [{ countPerDay: null }, { countPerDay: { $lt: 1.0 } }] },
                    { adult: false }
                ]
            }
        )
            .cache(CACHE_TIME)
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec();
    },

    listTV({ skip = 0, limit = 20, sort = "popularity" } = {}) {
        var sortobj = {};
        sortobj[sort] = -1;
        return this.find({ 'tv.id': { $ne: null } })
            .cache(CACHE_TIME)
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec()
    },
    listTVActive({ skip = 0, limit = 20, sort = "popularity" } = {}) {
        var sortobj = {};
        sortobj[sort] = -1;
        return this.find({ 'tv.id': { $ne: null }, 'tv.isActive': true })
            .cache(CACHE_TIME)
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec()
    },
    setActiveTV(tvID, isActive) {
        var query = { 'tv.id': tvID },
            update = { 'tv.isActive': isActive };
        return this.findOneAndUpdate(query, update)
            .exec()
            .then(result => { return next(result); })
    },
    removeAllTV(next) {
        return this.find({ 'tv.id': { $ne: null } })
            .then(movies => {
                movies.forEach(function (element) {
                    element.remove();
                }, this);
                return next();
            })
    },

    listByGenre({ skip = 0, limit = 20, sort = "popularity", group = 28 } = {}) {
        var sortobj = {};
        sortobj[sort] = -1;
        return this.find(
            {
                $and : [
                    { 'tv.id' : null},
                    {genre_ids: { "$in" : [group] }},
                    {vote_average : {$gt : 0}}
                ]
            })
            .cache(CACHE_TIME)
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec();
    },

    listTVByGenre({ skip = 0, limit = 20, sort = "popularity", group = 28 } = {}) {
        var sortobj = {};
        sortobj[sort] = -1;
        return this.find(
            {
                $and : [
                    { 'tv.id' : { $ne: null }},
                    {genre_ids: { "$in" : [group] }},
                    {vote_average : {$gt : 0}}
                ]
            })
            .cache(CACHE_TIME)
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec();
    },

    listByGenreHasStream({ skip = 0, limit = 20, sort = "popularity", group = 28 } = {}) {
        var sortobj = {};
        sortobj[sort] = -1;
        return this.find(
            {
                $and: [
                    { genre_ids: { "$in": [group] } },
                    { ggLinks: { $gt: [] } },
                    { $or: [{ countPerDay: null }, { countPerDay: { $lt: 1.0 } }] },
                    { adult: false }
                ]

            }
        )
            .cache(CACHE_TIME)
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec();
    },
    listTrailer({ skip = 0, limit = 20, sort = "popularity", group = 28 } = {}) {
        var sortobj = {};
        sortobj[sort] = -1;
        return this.find()
            .cache(CACHE_TIME)
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec();
    },

    searchV1({ skip = 0, limit = 20, query = "" } = {}) {
        if (query.length <= 2) {
            return this.search({ skip, limit, query });
        }

        var sortobj = {};
        sortobj["popularity"] = -1;
        return this.find(
            {
                $and: [
                    { $text: { $search: query } },
                    //{ $or:[{'tv.isActive' : true},{ ggLinks: { $gt: [] } }]},
                    { 'tv.id': null }
                ]
            },
            { score: { $meta: "textScore" } }
        )
            .sort({ score: { $meta: "textScore" } })
            .skip(skip)
            .limit(limit)
            .exec();
    },

    searchV2({ skip = 0, limit = 20, query = "" } = {}) {
        if (query.length <= 2) {
            return this.search({ skip, limit, query });
        }

        var sortobj = {};
        sortobj["popularity"] = -1;
        return this.find(
            { $text: { $search: query } },
            { score: { $meta: "textScore" } }
        )
            .sort({ score: { $meta: "textScore" } })
            .skip(skip)
            .limit(limit)
            .exec();
    },
    searchV3({ skip = 0, limit = 20, query = "" } = {}) {
        if (query.length <= 2) {
            return this.search({ skip, limit, query });
        }

        var sortobj = {};
        sortobj["popularity"] = -1;
        return this.find(
            {
                $and: [
                    { $text: { $search: query } },
                    { 'tv.id': null },
                    { adult: false },
                    { popularity: { $lt: 10 } }
                ]
            },
            { score: { $meta: "textScore" } }
        )
            .sort({ score: { $meta: "textScore" } })
            .skip(skip)
            .limit(limit)
            .exec();
    },

    searchV4({ skip = 0, limit = 20, query = "" } = {}) {
        if (query.length <= 2) {
            return this.search({ skip, limit, query });
        }

        var sortobj = {};
        sortobj["popularity"] = -1;
        return this.find(
            {
                $and: [
                    { $text: { $search: query } },
                    { 'tv.id': null },
                    { adult: false },
                    { popularity: { $lt: 10 } }
                ]
            },
            { score: { $meta: "textScore" } }
        )
            .sort({ score: { $meta: "textScore" } })
            .skip(skip)
            .limit(limit)
            .exec();
    },

    search({ skip = 0, limit = 20, query = "" } = {}) {
        var re = new RegExp(query, 'i');
        var sortobj = {};
        sortobj["popularity"] = -1;
        return this.find(
            {
                $and: [
                    { $or: [{ 'title': { $regex: re } }, { 'original_title': { $regex: re } }, { 'original_language': { $regex: re } }] },
                    { 'tv.id': null }
                ]
            })
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec();
    },
    searchHasStream({ skip = 0, limit = 20, query = "" } = {}) {
        var re = new RegExp(query, 'i');
        return this.find({
            $and: [
                { $or: [{ 'title': { $regex: re } }, { 'original_title': { $regex: re } }, { 'original_language': { $regex: re } }] },
                { $or: [{ 'tv.isActive': true }, { ggLinks: { $gt: [] } }] }
            ]
        })
            .cache(CACHE_TIME)
            .skip(skip)
            .limit(limit)
            .exec();
    },
    searchByTitle({ skip = 0, limit = 20, query = "" } = {}) {
        var re = new RegExp(query, 'i');
        return this.find({ 'title': { $regex: re } })
            .skip(skip)
            .limit(limit)
            .exec();
    },
    getRandOfLinks(id, next) {
        return this.findOne({ id: id })
            .exec()
            .then(movie => {
                if (movie) {
                    var idx = Math.floor((Math.random() * movie.ggLinks.length));
                    return next(movie.ggLinks[idx], movie);
                }
                return next(null, null);
            })
            .catch(e => {
                console.log(e);
                return next(null, null)
            }
            );

    },

    getRandDriveLink(id, next) {
        return this.findOne({ id: id })
            .exec()
            .then(movie => {
                if (movie) {
                    var idx = Math.floor((Math.random() * movie.ggdriveLinks.length));
                    return next(movie.ggdriveLinks[idx]);
                }
                return next(null);
            })
            .catch(e => {
                console.log(e);
                return next(null)
            }
            );
    },

    getLinks(id, next) {
        return this.findOne({ id: id })
            .exec()
            .then(movie => {
                if (movie) {
                    return next(movie.ggLinks);
                }
                return next(null);
            });
    },



    removeLink(id, link, next) {
        return this.update({ id: id }, { $pullAll: { ggLinks: [link] } })
            .exec()
            .then(result => {
                return next(result);
            })
    },

    removeLinkByLinkID(linkID, next) {
        return this.findOne({ ggLinks: { $in: [linkID] } })
            .then(movie => {
                return this.update({ ggLinks: { $in: [linkID] } }, { $pullAll: { ggLinks: [linkID] } })
                    .exec()
                    .then(results => {
                        console.log("movie = " + JSON.stringify(movie));
                        return next(movie);
                    })
            })

    },

    checkExist(id, next) {
        return this.count({ id: id })
            .exec()
            .then(count => {
                return next(count > 0);
            })
    },
    updateSubtile(id, subtitle, next) {
        var query = { id: id },
            update = { subtitle_identify: subtitle };
        return this.findOneAndUpdate(query, update)
            .exec()
            .then(result => { return next(result); })
    },
    updateAdult(id, isAudult, next) {
        console.log("isAudult = " + isAudult);
        var query = { id: id },
            update = { adult: isAudult };
        return this.findOneAndUpdate(query, update)
            .exec()
            .then(result => { return next(result); })
    },
    addViewCount(id, next) {
        var query = { id: id },
            update = { $inc: { viewCount: 1 } };
        return this.findOneAndUpdate(query, update)
            .exec()
            .then(result => {
                //console.log("result.countPerDay =  " + result.countPerDay );
                if (result.countPerDay == null || result.countPerDay < 0) {
                    result.countPerDay = 0;
                }
                if (result.ggLinks.length > 0)
                    result.countPerDay = result.countPerDay + 1.0 / result.ggLinks.length;

                if (result.viewCount == 4) {
                    result.createdAt = new Date();
                }

                result.save();
                return next(result);
            })
            .catch(e => { return next(null) });
    },
    resetCountPerDaylist(limit) {
        var sortobj = {};
        sortobj["countPerDay"] = -1;
        return this.find()
            .sort(sortobj)
            .limit(limit)
            .exec()
            .then(results => {
                results.forEach(element => {
                    element.countPerDay = 0;
                    element.save();
                });
            })
    },

    resetRequestCount(limit) {
        var sortobj = {};
        sortobj["requestCount"] = -1;
        return this.find()
            .sort(sortobj)
            .limit(limit)
            .exec()
            .then(results => {
                results.forEach(element => {
                    element.requestCount = 0;
                    element.save();
                });
            })
    },

    resetRequest(id, next) {
        return this.update({ tmvdbID: id }, { requestCount: 0 })
            .exec()
            .then(result => {
                return next(result);
            })
    },

    addRequest(id) {
        var query = { id: id },
            update = { $inc: { requestCount: 1 } };
        return this.findOneAndUpdate(query, update)
            .exec()
            .then(result => { return result; })
    },

    addDriveLink(tmvdbID, driveID, next) {
        return this.findOne({ tmvdbID: tmvdbID })
            .exec()
            .then((movie) => {
                if (movie) {
                    if (movie.ggdriveLinks.indexOf(driveID) < 0) {
                        movie.ggdriveLinks.push(driveID);
                        movie.save();
                    }
                    return next(movie);
                }
                return next(null)
            });
    },

    removeDriveLink(linkID, next) {
        return this.findOne({ ggdriveLinks: { $in: [linkID] } })
            .then(movie => {
                return this.update({ ggdriveLinks: { $in: [linkID] } }, { $pullAll: { ggdriveLinks: [linkID] } })
                    .exec()
                    .then(results => {
                        console.log("remove drive link at  = " + JSON.stringify(movie));
                        return next(movie);
                    })
            })

    },

    getViewPerDay() {
        return this.find({ countPerDay: { $gt: 0 } })
            .exec()
            .then(results => {
                var total = 0;
                results.forEach(element => {
                    if (element.countPerDay && element.ggLinks) {
                        total = total + element.countPerDay * element.ggLinks.length;
                    }
                });
                return total;
            });
    },

    getListHasLink({ skip = 0, limit = 20, sort = "popularity" } = {}) {
        // let data = []
        var sortobj = {};
        sortobj[sort] = -1;
        console.log("getListHasLink");

        // var Movie = mongoose.model('Movie', MovieSchema);
        // return Movie.aggregate([
        //     { $lookup: { from: 'clientlinks', localField: 'id', foreignField: 'movieID', as: 'client' }},
        //     { $project : { id : 1,}}
        //   ]).
        //   sort(sortobj).
        //   skip(skip).
        //   limit(limit).
        //   then(function (res) {
        //     return res;
        //   });

        this.find({}).populate('clientlinks', { id: movieID }).exec()
    }
};
export default mongoose.model('Movie', MovieSchema);

