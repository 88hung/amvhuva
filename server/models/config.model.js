import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * User Schema
 */
const ConfigSchema = new mongoose.Schema({
  packageName: {
    type: String,
    default: "com.huva.abcd"
  },
  versionCode: {
    type: Number,
    default: 0
  },
  configs: {
    type: Object,
    default: {
      "os_type": "android",
      "subtile_server": "http://fakeSub.com",
      "is_showTrailer": false,
      "is_show_ads": true,
      "is_show_full_screen_ads": true,
      "defaultView": 10,
      "mode": 2,
      "admob": {
        "app_id": "ca-app-pub-9572710061084973~7373227842",
        "banner": "ca-app-pub-9572710061084973/7415097040",
        "interstitial": "ca-app-pub-9572710061084973/2733995627",
        "rewardVideo": "ca-app-pub-9572710061084973/7361482254",
        "nativeExpress": "ca-app-pub-9572710061084973/3955429846",
        "showVideoAdPercent": 100,
        "showIntertisialPercent": 30
      },
      "chartBoost": {
        "enable": false,
        "app_id": "5991094ef6cd451a0ce62643",
        "signature": "e298b0d8f4dd743ef14db8666a9851e4cbed25e1"
      },
      "vungle": {
        "enable": true,
        "app_id": "598c150d9fa26d1d690030b9",
        "report_id": "598c150d9fa26d1d690030b9",
        "placement_ref_id": "DEFAULT51196"
      },
      "unity_ads": {
        "enable": true,
        "game_id": "1511072"
      },
      "adcolony": {
        "enable": true,
        "app_id": "app781aff5fbdd143dba7",
        "zone_id": "vz38f5f8e561ed454c86"
      },
      "force": {
        "package_name": "",
        "external_link": "",
        "keep_current_version": true,
        "description": ""
      },
      "promotions": [],
      "sub_title": {
        "enable": true,
        "regex1": "",
        "regex2": ""
      }
    }
  }

});

ConfigSchema.method({
});

/**
 * Statics
 */
ConfigSchema.statics = {
  get(packageName, versionCode, next) {
    return this.findOne({ packageName: packageName, versionCode: versionCode })
      .exec()
      .then((config) => {
        if (config) {
          return next(config);
        }
        return next(null);
      });
  },
  list(next) {
    return this.find()
      .exec()
      .then(list => {
        return next(list);
      })
  },
  set(packageName, versionCode, configs, next) {
    var query = { packageName: packageName, versionCode: versionCode },
      update = { configs: configs },
      options = { upsert: true, new: true, setDefaultsOnInsert: true };

    return this.findOneAndUpdate(query, update, options)
      .exec()
      .then(result => { return next(result); })
  },
  //   remove(movieID,linkID)
  //   {
  //     return this.findOne({movieID : movieID, linkID : linkID})
  //     .remove()
  //     .exec()
  //   }
};
export default mongoose.model('Configs', ConfigSchema);
