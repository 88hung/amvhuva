const mongoose = require('mongoose')
const AutoIncrement = require('mongoose-sequence')(mongoose);

const ObjectId = mongoose.Schema.Types.ObjectId;

// tạo cấu trúc db
const SeriesSchema = mongoose.Schema({
    id: {
        type: Number
    },
    chaps: [
        {
            ep: Number,
            ggLinks: [ObjectId],
            subtitle: String
        }
    ]
});

SeriesSchema.statics = {
    get(movieId) {
        console.log("movieId" + movieId)
        return this.findOne({ movieID: movieId })
            .exec()
            .then(result => result)
    }
}

export default mongoose.model('Series', SeriesSchema);