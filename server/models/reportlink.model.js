import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * User Schema
 */
const ReportLinkSchema = new mongoose.Schema({
  linkID: {
    type: String,
    default: ""
  },
  movieID: {
    type: String,
    default: ""
  },
  count: {
    type: Number,
    default: 0
  }
});

ReportLinkSchema.method({
});

/**
 * Statics
 */
ReportLinkSchema.statics = {
  get(movieID, linkID, next) {
    return this.findOne({ movieID: movieID, linkID: linkID })
      .exec()
      .then((report) => {
        if (report) {
          return next(report);
        }
        return next(null);
      });
  },
  set(movieID, linkID, next) {
    var query = { movieID: movieID, linkID: linkID },
      update = { $inc: { count: 1 } },
      options = { upsert: true, new: true, setDefaultsOnInsert: true };

    return this.findOneAndUpdate(query, update, options)
      .exec()
      .then(result => { return next(result); })
  },
  remove(movieID, linkID) {
    return this.findOne({ movieID: movieID, linkID: linkID })
      .remove()
      .exec()
  }
};
export default mongoose.model('ReportLink', ReportLinkSchema);
