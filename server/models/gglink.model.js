
import mongoose from 'mongoose';

const ObjectId = mongoose.Schema.Types.ObjectId;

const GGLinkSchema = mongoose.Schema({
  email: {
    type: String,
    default: ""
  },
  url: {
    type: String,
    default: ""
  },
  streams: {
    type: Array,
    default: []
  },
  viewCount: {
    type: Number,
    default: 0
  },
  status: {
    type: Boolean,
    default: true
  },
  lastAccess: {
    type: Date,
    default: Date.now
  }
});


GGLinkSchema.statics = {
  get(id) {
    return this.findById(id)
      .exec()
      .then((user) => {
        if (user) {
          return user;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },
  getByUrl(url) {
    return this.findOne({ url: url })
      .exec()
      .then((user) => {
        if (user) {
          return user;
        }
        return null;
      });
  },
  getStreams(id, next) {
    return this.findById(id)
      .exec()
      .then((link) => {
        if (link) {
          return next(link);
        }
        return next(null);
      })
      .catch(e => {
        console.log("getStreams(id,next) {2" + id);
        return next(null);
      });
  },

  countAll() {
    return this.count()
      .exec()
      .then(count => count);
  },

  getViewCount(id, next) {
    return this.findById(id)
      .exec()
      .then((link) => {
        if (link) {
          return next(link.viewCount);
        }
        return next(0);
      });
  },
  listall() {
    return this.find()
      .exec()
      .then((gglinks) => {
        if (gglinks) {
          return gglinks;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },
  listStatusFalse() {
    return this.find({ status: false })
      .exec()
      .then((gglinks) => {
        if (gglinks) {
          return gglinks;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },
  list(ids) {
    return this.find({ _id: { $in: ids } })
      .exec()
      .then((gglinks) => {
        if (gglinks) {
          return gglinks;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },
  createOrUpdate(email, url, videos, next) {
    var query = { url: url },
      update = { email: email, url: url, streams: videos },
      options = { upsert: true, new: true, setDefaultsOnInsert: true };

    return this.findOneAndUpdate(query, update, options)
      .exec()
      .then(result => { return next(result); })
      .catch(e => next(null))
  },

  addViewCount(linkID) {
    var query = { _id: linkID },
      update = { $inc: { viewCount: 1 } };
    return this.findOneAndUpdate(query, update)
      .exec()
      .then(result => { return result; })
      .catch(e => null)
  },

  removeLink(link, next) {
    return this.findOne({ url: link })
      .exec()
      .then(result => {
        var id = "";
        var link = "";
        var mail = "";
        var viewCount = 0;
        if (result) {
          id = result._id;
          link = result.url;
          mail = result.email;
          viewCount = result.viewCount;
          result.remove();
        }
        return next(id, link, mail, viewCount);
      })
  },
  removeLinkByID(id, next) {
    return this.findById(id)
      .exec()
      .then(result => {
        var id = "";
        var link = "";
        var mail = "";
        var viewCount = 0;
        if (result) {
          id = result._id;
          link = result.url;
          mail = result.email;
          viewCount = result.viewCount;
          result.remove();
        }
        return next(id, link, mail, viewCount);
      })
  },
  updateStatus(id, status, next) {
    return this.findById(id)
      .exec()
      .then(link => {
        if (link) {
          link.status = status;
          link.save();
        }
        return next(link);
      })
      .catch(e => next(null));
  },
};

export default mongoose.model('GGLink', GGLinkSchema);
