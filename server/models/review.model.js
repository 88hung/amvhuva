
const mongoose = require('mongoose');

const ObjectId = mongoose.Schema.Types.ObjectId;

const review = mongoose.Schema({
  id: {
    type: Number,
    default: ""
  },
  page: {
    type: Number,
    default: 1
  },
  total_pages: {
    type: Number,
    default: 1
  },
  total_results: {
    type: Number,
    default: 1
  },
  results: {
    type: Array,
    default: []
  }
});

review.statics = {
  getByMovie(id) {
    return this.findOne({ id: id })
      .exec()
      .then(review => review)
  },
  list() {
    return this.find()
      .exec()
      .then(reviews => reviews);
  }
}
export default mongoose.model('Review', review);
