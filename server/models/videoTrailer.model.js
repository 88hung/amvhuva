
import mongoose from 'mongoose';

const ObjectId = mongoose.Schema.Types.ObjectId;

const VideoTrailerSchema = mongoose.Schema({
  movieID: {
    type: String,
    default: 0
  },
  results: [{
    key: {
      type: String,
      default: 'c38r-SAnTWM'
    },
    name: {
      type: String,
      default: 'Official US Teaser Trailer'
    },
    site: {
      type: String,
      default: ''

    },
    size: { 
      type: Number,
      default: 720

    },
    type: {
      type: String,
      default: 'Trailer'

    }
  }]
});

VideoTrailerSchema.statics = {
    /**
     * Get user
     * @param {ObjectId} id - The objectId of user.
     * @returns {Promise<User, APIError>}
     */
  get(id) {
    return this.findById(id)
        .exec()
        .then((user) => {
          if (user) {
            return user;
          }
          const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
  },
  listvideo(id) {
    return this.findOne({movieID : id})
        .exec()
        .then((videos) => {
          if (videos) {
            return videos;
          }
          return null;
        })
        .catch(e => {return null})
  },
  list() {
    return this.find()
        .exec()
        .then((videos) => {
          if (videos) {
            return videos;
          }
          const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
      
      });
  },
  createOrUpdate(movieID,results){
    var query = {movieID : movieID},
    update = {results : results},
    options = { upsert: true, new: true, setDefaultsOnInsert: true };
    return this.findOneAndUpdate(query,update,options)
    .exec()
    .then(result => result)
  }
};

export default mongoose.model('VideoTrailer', VideoTrailerSchema);
