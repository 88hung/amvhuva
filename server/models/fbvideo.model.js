
import mongoose from 'mongoose';

const ObjectId = mongoose.Schema.Types.ObjectId;

const FbVideo = mongoose.Schema({
  movieID: {
    type: String,
    default: ""
  },
  fbID: {
    type: String,
    default: ""
  },
  videoFBID: {
    type: String,
    default: ""
  },
  viewCount: {
    type: Number,
    default: 0
  },
  streams: {
    type: Array,
    default: []
  }
});

FbVideo.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(movieID) {
    return this.find({ "movieID": movieID })
      .exec()
      .then((movies) => {
        return movies;
      });
  },

  list() {
    return this.find({})
      .exec();
  },


  addViewCount(linkID) {
    var query = { _id: linkID },
      update = { $inc: { viewCount: 1 } };
    return this.findOneAndUpdate(query, update)
      .exec()
      .then(result => { return result; })
      .catch(e => null)
  },

  getViewCount(id, next) {
    return this.findById(id)
      .exec()
      .then((link) => {
        if (link) {
          return next(link.viewCount);
        }
        return next(0);
      });
  },

  removeLink(link, next) {
    return this.findOne({ url: link })
      .exec()
      .then(result => {
        var id = "";
        var link = "";
        var mail = "";
        var viewCount = 0;
        if (result) {
          id = result._id;
          link = result.url;
          mail = result.email;
          viewCount = result.viewCount;
          result.remove();
        }
        return next(id, link, mail, viewCount);
      })
  },

  createOrUpdate(movieID, fbID, videoID, streams, next) {
    var query = { movieID: movieID, fbID: fbID },
      update = { videoID: videoID, streams: streams },
      options = { upsert: true, new: true, setDefaultsOnInsert: true };

    return this.findOneAndUpdate(query, update, options)
      .exec()
      .then(result => { return next(result); })
      .catch(e => next(null))
  },

  getRandomLink(movieID, next) {
    return this.findOne({ movieID: movieID })
      .exec()
      .then(video => {
        if (video) {
          var idx = Math.floor((Math.random() * video.links.length));
          return next(video.links[idx]);
        }
        return next(null);
      })
      .catch(e => {
        console.log(e);
        return next(null)
      }
      );
  },
  incsParseCount(movieID, GGLink, next) {
    var query = { movieID: movieID, GGLink: GGLink },
      update = { $inc: { parseCount: 1 } };
    return this.findOneAndUpdate(query, update)
      .exec()
      .then(result => next(result))
      .catch(e => next(null));
  },
};

export default mongoose.model('FbVideo', FbVideo);
