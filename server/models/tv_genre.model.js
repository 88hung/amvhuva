const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;


// tạo cấu trúc db
const TVGenreSchema = mongoose.Schema({
  id: {
    type: Number,
    default: 0
  },
  name: {
    type: String,
    default: 'Action'
  },
});

TVGenreSchema.statics = {
    /**
     * Get user
     * @param {ObjectId} id - The objectId of user.
     * @returns {Promise<User, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((genre) => {
          if (genre) {
            return genre;
          }
          const err = new APIError('No such genre exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List users in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of users to be skipped.
     * @param {number} limit - Limit number of users to be returned.
     * @returns {Promise<User[]>}
     */
    list() {
      return this.find({}, { _id: 0 ,__v : 0})
        .exec();
    }
  };
  
export default mongoose.model('TVGenre', TVGenreSchema);