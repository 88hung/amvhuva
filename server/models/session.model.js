const mongoose = require('mongoose')
const AutoIncrement = require('mongoose-sequence')(mongoose);

const ObjectId = mongoose.Schema.Types.ObjectId;

// tạo cấu trúc db
const SessionSchema = mongoose.Schema({
    movieID: {
        type: Number,
        default: 0
    },
    tvID: {
        type: Number,
        default: 0
    },
    id: {
        type: Number
    },
    poster_path: {
        type: String,
        default: ""
    },
    season_number: {
        type: Number,
        default: 0
    },
    episode_count: {
        type: Number
    },
    series: [
        {
            ep: Number,
            ggLinks: [ObjectId],
            subtitle: String
        }
    ],
    air_date: {
        type: Date
    }
});

SessionSchema.statics = {
    get(tvID) {
        console.log("movieId" + tvID)
        return this.find({ tvID: tvID })
            .exec()
            .then(result => result)
    },
    getByTv(tvID, sessionID) {
        return this.findOne({ tvID: tvID, id: sessionID })
            .exec()
            .then(result => result)
    },
    createOrUpdate(newsession, next) {
        var query = { id: newsession.id },
            update = { air_date: newsession.air_date, episode_count: newsession.episode_count, season_number: newsession.season_number, tvID: newsession.tvID, movieID: newsession.movieID },
            options = { upsert: true, new: true, setDefaultsOnInsert: true };

        return this.findOneAndUpdate(query, update, options)
            .exec()
            .then(result => { return next(result); })
            .catch(e => next(null))
    },

    removeSeriesByID(linkID, next) {
        return this.update({ 'series.ggLinks': { $in: [linkID] } }, { $pullAll: { 'series.$.ggLinks': [linkID] } })
            .exec()
            .then(results => {
                return next(results);
            })
    }
}

export default mongoose.model('Session', SessionSchema);