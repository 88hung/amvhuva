
const mongoose = require('mongoose')
const AutoIncrement = require('mongoose-sequence')(mongoose);

const ObjectId = mongoose.Schema.Types.ObjectId;

// tạo cấu trúc db
const TrailerSchema = mongoose.Schema({
    vote_count: {
        type: Number,
        default: 0
    },
    tmvdbID: {
        type: Number,
        default: 0
    },
    video:
    {
        type: Boolean,
        default: false
    },
    tv: {
        id: {
            type: Number
        },
        sessions: {
            type: Array
        },
        isActive: {
            type: Boolean,
            default: false
        }
    },
    vote_average: {
        type: Number,
        default: 0
    },
    title: {
        type: String,
        default: 'Passage of Venus'
    },
    popularity: {
        type: Number,
        default: 0.0
    },
    poster_path:
    {
        type: String,
        default: '/9jtgJphKW836gISyzdUKWnZ6yrB.jpg'
    },
    original_language: {
        type: String,
        default: 'en'
    },
    original_title:
    {
        type: String,
        default: 'Passage of Venus'
    },
    genre_ids:
    {
        type: Array,
        default: []
    },
    backdrop_path:
    {
        type: String,
        default: '/9jtgJphKW836gISyzdUKWnZ6yrB.jpg'
    },
    adult: {
        type: Boolean,
        default: false
    },
    overview:
    {
        type: String,
        default: 'Photo sequence of the rare transit of Venus over the face of the Sun, one of the first chronophotographic sequences.'
    },
    release_date:
    {
        type: Date,
        default: Date.now
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    ggLinks: {
        type: [ObjectId],
        default: []
    },
    status: {
        type: Number,
        default: 0// inactive 1 active 2 die 
    },
    subtitle_identify: {
        type: String,
        default: ""
    },
    viewCount: {
        type: Number,
        default: 0
    },
    requestCount: {
        type: Number,
        default: 0
    },
    id: {
        type: Number,
        default: 0
    },
}, { _v: false });

TrailerSchema.statics = {
    /**
     * Get user
     * @param {ObjectId} id - The objectId of user.
     * @returns {Promise<User, APIError>}
     */
    get(id, next) {
        return this.findOne({ id: id })
            .exec()
            .then((movie) => {
                if (movie) {
                    return next(movie);
                }
                return next(null)
            });
    },

    getbyTMVDB(tmvdbID, next) {
        return this.findOne({ tmvdbID: tmvdbID })
            .exec()
            .then((movie) => {
                if (movie) {
                    return next(movie);
                }
                return next(null)
            });
    },
    getbyTV(tvID, next) {
        return this.findOne({ 'tv.id': tvID })
            .exec()
            .then((movie) => {
                if (movie) {
                    return next(movie);
                }
                return next(null)
            });
    },
    set(newMovie) {
        return this.findOne({ id: newMovie.id })
            .exec()
            .then((movie) => {
                if (movie) {
                    movie.doc = newMovie;
                    movie.save();
                }
                else {
                    var Trailer = mongoose.model('Trailer', TrailerSchema);
                    var movieSave = new Trailer(newMovie);
                    movieSave.save();
                }
            });
    },

    setIsSeries(movieID) {
        return this.findOne()
    },
    /**
     * List users in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of users to be skipped.
     * @param {number} limit - Limit number of users to be returned.
     * @returns {Promise<User[]>}
     */

    list({ skip = 0, limit = 20, sort = "popularity" } = {}) {
        var sortobj = {};
        sortobj[sort] = -1;
        return this.find()
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec()
            .then(result => {
                result.forEach(function (element) {
                    element.backdrop_path = "";
                    element.poster_path = "";
                }, this);
                return result;
            });
    },
    listHasStreams({ skip = 0, limit = 20, sort = "popularity" } = {}) {
        var sortobj = {};
        sortobj[sort] = -1;
        return this.find({ $or: [{ 'tv.isActive': true }, { ggLinks: { $gt: [] } }] })
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec();
    },

    listTV({ skip = 0, limit = 20, sort = "popularity" } = {}) {
        var sortobj = {};
        sortobj[sort] = -1;
        return this.find({ 'tv.id': { $ne: null } })
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec()
    },
    listTVActive({ skip = 0, limit = 20, sort = "popularity" } = {}) {
        var sortobj = {};
        sortobj[sort] = -1;
        return this.find({ 'tv.id': { $ne: null }, 'tv.isActive': true })
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec()
    },
    setActiveTV(tvID, isActive) {
        var query = { 'tv.id': tvID },
            update = { 'tv.isActive': isActive };
        return this.findOneAndUpdate(query, update)
            .exec()
            .then(result => { return next(result); })
    },
    removeAllTV(next) {
        return this.find({ 'tv.id': { $ne: null } })
            .then(movies => {
                movies.forEach(function (element) {
                    element.remove();
                }, this);
                return next();
            })
    },

    listByGenre({ skip = 0, limit = 20, sort = "popularity", group = 28 } = {}) {
        var sortobj = {};
        sortobj[sort] = -1;
        return this.find({ genre_ids: { "$in": [group] } })
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec();
    },
    listByGenreHasStream({ skip = 0, limit = 20, sort = "popularity", group = 28 } = {}) {
        var sortobj = {};
        sortobj[sort] = -1;
        return this.find({ genre_ids: { "$in": [group] }, ggLinks: { $gt: [] } })
            .sort(sortobj)
            .skip(skip)
            .limit(limit)
            .exec();
    },
    search({ skip = 0, limit = 20, query = "" } = {}) {
        var re = new RegExp(query, 'i');
        return this.find()
            .or([{ 'title': { $regex: re } }, { 'original_title': { $regex: re } }, { 'original_language': { $regex: re } }]) //,{ 'overview': { $regex: re }}
            .skip(skip)
            .limit(limit)
            .exec();
    },
    searchByTitle({ skip = 0, limit = 20, query = "" } = {}) {
        var re = new RegExp(query, 'i');
        return this.find({ 'title': { $regex: re } })
            .skip(skip)
            .limit(limit)
            .exec();
    },
};
export default mongoose.model('Trailer', TrailerSchema);

