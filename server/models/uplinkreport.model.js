import mongoose from 'mongoose';

const ObjectId = mongoose.Schema.Types.ObjectId;

const GGLinkSchema = mongoose.Schema({
    email : {
        type : String,
        default : ""
    },
    success : {
        type : Number,
        default : 0
    },
    failed : {
        type : Number,
        default : 0
    }
});
