import { EILSEQ } from 'constants';

const requestIp = require('request-ip');
var async = require('async');
const https = require('https');

const BANNED_TTL = 60 * 60 * 24 * 7;
const MV_TTL = 60 * 60 * 24 * 7;
const GL_TTL = 60 * 5;
const SEARCH_TTL = 60 * 5;
const GMVL_TTL = 60 * 60 * 24 * 7;
const GCF_TTL = 60 * 60 * 24 * 7;

const LIMIT_REQUEST = 20;

const query_prefix = "Q_IP";
const report_prefix = "RP_";
var redis = require("redis");
var subscriberClient = redis.createClient();
var schedQueueClient = redis.createClient();

// // when a published message is received...
// subscriberClient.on("pmessage", function (pattern, channel, expiredKey) {
//     console.log("channel ["+  channel +"] has expired");
//     console.log("key ["+  expiredKey +"] has expired");
//     if(expiredKey.indexOf(query_prefix) != -1)
//     {
//         schedQueueClient.get(expiredKey,function(err,value){
//             console.log("Save value = " + value);
//             schedQueueClient.set(report_prefix + expiredKey,value,function(err,reply){
//                 console.log("Save to report = " + reply);
//             });
//         }); 
//     }
// });

// subscriberClient.psubscribe("__keyevent@0__:expired");
function reportToSlack(ip, count) {
    var url = "https://slack.com/api/chat.postMessage?token=xoxp-242080693313-261025019845-267157917989-839784e5b2fcf9dd569db1fc34a23211&channel=C8FPN750B&text="
        + "ip:" + ip
        + "-count:" + count
        + "&pretty=1";

    https.get(url, (resp) => {
        let data = '';
        resp.on('data', (chunk) => {
            data += chunk;
        });
        resp.on('end', () => {
            console.log("Sent to slack link " + ip + "count " + count);
        });

    }).on("error", (err) => {
        console.log("Error: " + err.message);
    });
}

function add(api, ip, next, timeToLive) {
    var key = "IPC_" + ip;
    return schedQueueClient.incr(key, next);

    //console.log("key = " + key);
    // schedQueueClient.get(key,function(err,count){
    //     if(count)
    //     {
    //         if(count >= LIMIT_REQUEST)
    //         {
    //             if(api === "GL_" || api === "SMV_")
    //             {
    //                 if(schedQueueClient.get(report_prefix + key,function(err,reply){
    //                     console.log("get rp reply = " + reply);
    //                     if(reply)
    //                     {
    //                         console.log("banned " + key + " : " + reply);
    //                     }
    //                     else
    //                     {
    //                         console.log("send to slack " + report_prefix + key);
    //                         schedQueueClient.set(report_prefix + key,count,'EX',BANNED_TTL, function(err,reply){});
    //                         reportToSlack(report_prefix + key,count);
    //                     }
    //                 }));
    //             }
    //         }
    //         return schedQueueClient.incr(key,next);
    //     }
    //     else
    //     {
    //         return schedQueueClient.set(key,1,'EX',timeToLive,next);
    //     }
    // })
}

function current_list(req, res, next) {
    var jobs = [];
    var pattern = req.headers.pattern;
    return schedQueueClient.keys(pattern, function (err, keys) {
        if (err) return res.json(err);
        if (keys) {
            return async.map(keys, function (key, cb) {
                schedQueueClient.get(key, function (error, value) {
                    if (error)
                        return cb(error);
                    var job = {};
                    job['key'] = key;
                    job['value'] = value;
                    cb(null, job);
                });
            }, function (error, results) {
                if (error)
                    return res.json(error);
                //console.log(results);
                results.sort(function (a, b) {
                    var countA = parseInt(a.count);
                    var countB = parseInt(b.count);
                    if (countA < countB)
                        return 1;
                    if (countA > countB)
                        return -1;
                    return 0;
                });
                return res.json(results);
            });
        }
    });
}

function report_list(req, res, next) {
    var jobs = [];
    var pattern = req.headers.pattern;
    schedQueueClient.keys(pattern, function (err, keys) {
        if (err) return res.json(err);
        if (keys) {
            return async.map(keys, function (key, cb) {
                schedQueueClient.get(key, function (error, value) {
                    if (error)
                        return cb(error);
                    cb(null, { 'ip': key, 'count': value });
                });
            }, function (error, results) {
                if (error)
                    return res.json(error);
                results.sort(function (a, b) {
                    var countA = parseInt(a.count);
                    var countB = parseInt(b.count);
                    if (countA < countB)
                        return 1;
                    if (countA > countB)
                        return -1;
                    return 0;
                });
                return res.json(results);
            });
        }
    });
}


// inside middleware handler
const ipGetLinkMiddleware = function (req, res, next) {
    const clientIp = requestIp.getClientIp(req);
    //console.log("clientIp" + clientIp);
    add("GL_", clientIp, function (reply) { }, GL_TTL);

    return next();

    // var key = report_prefix + "GL_" + query_prefix + clientIp;
    // if(schedQueueClient.get(key,function(error,reply){
    //     if(reply)
    //     {
    //         //return res.json("abc xyz");
    //         console.log("HACK = " + key);
    //         return next();
    //     }
    //     else
    //     {
    //         return next();
    //     }
    // }));
};

// inside middleware handler
const ipViewMovieMiddleware = function (req, res, next) {
    const clientIp = requestIp.getClientIp(req);
    //console.log("clientIp" + clientIp);
    add("VM_", clientIp, function (reply) { }, MV_TTL);
    next();
};

const ipGetConfigMiddleware = function (req, res, next) {
    const clientIp = requestIp.getClientIp(req);
    //console.log("clientIp" + clientIp);
    add("GCF_", clientIp, function (reply) { }, GCF_TTL);
    next();
};

const ipGetMVListMiddleware = function (req, res, next) {
    const clientIp = requestIp.getClientIp(req);
    //console.log("clientIp" + clientIp);
    add("GMVL_", clientIp, function (reply) { }, GMVL_TTL);
    next();
};

const ipSearchMVMiddleware = function (req, res, next) {
    const clientIp = requestIp.getClientIp(req);
    //console.log("clientIp : " + clientIp+ " query = " + req.query.query );
    add("SMV_", clientIp, function (reply) { }, SEARCH_TTL);
    next();
};

function clear(req, res, next) {
    schedQueueClient.flushdb(function (err, succeeded) {
        console.log(succeeded); // will be true if successfull
    });
    return res.json("done");
}

export default {
    ipGetLinkMiddleware,
    ipViewMovieMiddleware,
    ipGetConfigMiddleware,
    ipGetMVListMiddleware,
    ipSearchMVMiddleware,
    clear,
    current_list,
    report_list
};