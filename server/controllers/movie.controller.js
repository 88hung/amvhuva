import Movie from '../models/movie.model';
import Review from '../models/review.model';
import Genre from '../models/genre.model';
import TVGenre from '../models/tv_genre.model';
import Video from '../models/video.model';
import GGLink from '../models/gglink.model';
import Report from '../models/reportlink.model';
import Session from '../models/session.model';
import Trailer from '../models/trailer.model';
import VideoTrailer from '../models/videoTrailer.model';
import AdminCtrl from "./movie.admin.controller";
import ggCtrl from '../controllers/googlePhotoParser.controller';
import ClientLink from '../models/client.link.model';

var request = require('request');

function create(req, res, next) {
  const movie = new Movie({
    // username: req.body.username,
    // mobileNumber: req.body.mobileNumber
  });

  movie.save()
    .then(savedMovie => res.json(savedMovie))
    .catch(e => next(e));
}

function update(req, res, next) {
  // const user = req.movie;
  // user.username = req.body.username;
  // user.mobileNumber = req.body.mobileNumber;

  // user.save()
  //   .then(savedUser => res.json(savedUser))
  //   .catch(e => next(e));
  next('not implement yet!');
}
function listHasLink(req, res, next) {
  var mode = 1;
  if (req.headers.mode)
    mode = parseInt(req.headers.mode);

  var page = parseInt(req.query.page) - 1;
  var perPage = 20;
  var sort_by = req.query.sort_by.split(".")[0];
  if (sort_by == "revenue") {
    sort_by = "createdAt";
  }

  const query = { skip: page * perPage, limit: perPage, sort: sort_by };

  var movies = Movie.getListHasLink(query);
  return res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies })

}

function list(req, res, next) {
  var mode = 1;
  if (req.headers.mode)
    mode = parseInt(req.headers.mode);

  var page = parseInt(req.query.page) - 1;
  var perPage = 20;
  var sort_by = req.query.sort_by.split(".")[0];
  if (sort_by == "revenue") {
    sort_by = "createdAt";
  }

  const query = { skip: page * perPage, limit: perPage, sort: sort_by };
  if (mode == 0) {
    if (sort_by == "createdAt") {
      return Movie.listNew(query)
        .then(movies => res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies }))
        .catch(e => next(e));
    }
    else {
      return Movie.list(query)
        .then(movies => res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies }))
        .catch(e => next(e));
    }
  }
  else if (mode == 1) {

    return Movie.listHasStreams(query)
    .then(movies => res.json({page:parseInt(req.query.page),total_pages:-1,total_results:-1,"results":movies}))
    .catch(e => next(e));


    // return Movie.getListHasLink(query)
    //   .then(movies => res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies }))
    //   .catch(e => next(e));
  }
  else if (mode == 2) {
    return Trailer.list(query)
      .then(movies => res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies }))
      .catch(e => next(e));
  }
  else if (mode == 3) {
    if (sort_by == "createdAt") {
      return Movie.listNew(query)
        .then(movies => {
          movies.forEach(element => {
            element.backdrop_path = "";
            element.poster_path = "";
          });
          res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies })
        })
        .catch(e => next(e));
    }
    else {
      return Movie.listHasStreams(query)
        .then(movies => {
          movies.forEach(element => {
            element.backdrop_path = "";
            element.poster_path = "";
          });
          res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies })
        })
        .catch(e => next(e));
    }
  }
}
function listtv(req, res, next) {
  var page = parseInt(req.query.page) - 1;
  var mode = 1;
  if (req.headers.mode)
    mode = parseInt(req.headers.mode);
  var perPage = 20;
  var sort_by = req.query.sort_by.split(".")[0];
  const query = { skip: page * perPage, limit: perPage, sort: sort_by };
  if (mode == 0) {
    return Movie.listTV(query)
      .then(movies => res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies }))
      .catch(e => next(e));
  } else if (mode == 1) {
    return Movie.listTVActive(query)
      .then(movies => res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies }))
      .catch(e => next(e));
  } else if (mode == 2) {
    return Trailer.list(query)
      .then(movies => res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies }))
      .catch(e => next(e));
  } else if (mode == 3) {
    return Movie.listTVActive(query)
      .then(movies => {
        movies.forEach(movie => {
          movie.backdrop_path = "";
          movie.poster_path = "";
        });
        res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies })
      })
      .catch(e => next(e));
  }
}


function listTVbyGenre(req, res, next) {
  var page = parseInt(req.query.page) - 1;
  var genreID = parseInt(req.query.genreID)
  var perPage = 20;
  const query = { skip: page * perPage, limit: perPage, sort: "popularity", group: genreID };
  var mode = 1;
  if (req.headers.mode)
    mode = parseInt(req.headers.mode);
  if (mode == 0) {
    return Movie.listTVByGenre(query)
      .then(movies => res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies }))
      .catch(e => next(e));
  } else {
    return Trailer.list(query)
      .then(movies => res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies }))
      .catch(e => next(e));
  }
}
function listbyGenre(req, res, next) {
  var page = parseInt(req.query.page) - 1;
  var genreID = parseInt(req.query.genreID)
  var perPage = 20;
  const query = { skip: page * perPage, limit: perPage, sort: "popularity", group: genreID };
  var mode = 1;
  if (req.headers.mode)
    mode = parseInt(req.headers.mode);
  if (mode == 0) {
    return Movie.listByGenre(query)
      .then(movies => res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies }))
      .catch(e => next(e));
  }
  else if (mode == 1) {
    return Movie.listByGenreHasStream(query)
      .then(movies => res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies }))
      .catch(e => next(e));
  } else if (mode == 2) {
    return Trailer.list(query)
      .then(movies => res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies }))
      .catch(e => next(e));
  } else if (mode == 3) {
    return Movie.listByGenre(query)
      .then(movies => {
        movies.forEach(movie => {
          movie.backdrop_path = "";
          movie.poster_path = "";
        });
        res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies })
      })
      .catch(e => next(e));
  }

}

// @GET("/movie/search") Observable<Movie.Response> discoverMoviesByQuery(
//         @Query("query") String search_query);

var async = require('async');
const https = require('https');
var redis = require("redis");
var schedQueueClient = redis.createClient();

function search(req, res, next) {
  var page = parseInt(req.query.page) - 1;
  var perPage = 20;
  var mode = 1;

  if (req.headers.mode)
    mode = parseInt(req.headers.mode);

  if (req.query.query.length == 0)
    return res.json({});

  const query = { skip: page * perPage, limit: perPage, query: req.query.query };
  if (mode == 1) {

    return Movie.searchV1(query)
      .then(movies => {
        return res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies })
      }
      )
      .catch(e => {
        console.log("BI LOI searchV1");
        return next(e);
      });

    //   const clientIp = requestIp.getClientIp(req);
    //   return schedQueueClient.get("IPC_" + clientIp ,function(err,reply){
    //     console.log("reply = " + reply);
    //     if(reply && reply > 100)
    //     {
    //       return Movie.searchV1(query)
    //       .then(movies => {
    //          return res.json({page:parseInt(req.query.page),total_pages:-1,total_results:-1,"results":movies})}
    //         )
    //       .catch(e => {
    //         console.log("BI LOI searchV1");
    //         return next(e);
    //       });
    //     }
    //     else if(reply && reply > 15)
    //     {
    //       return Movie.searchV2(query)
    //       .then(movies => {
    //          return res.json({page:parseInt(req.query.page),total_pages:-1,total_results:-1,"results":movies})}
    //         )
    //         .catch(e => {
    //           console.log("BI LOI searchV2");
    //           return next(e);
    //         });
    //     }
    //     else
    //     {
    //       return Movie.searchV3(query)
    //       .then(movies => {
    //          return res.json({page:parseInt(req.query.page),total_pages:-1,total_results:-1,"results":movies})}
    //         )
    //         .catch(e => {
    //           console.log("BI LOI searchV3");
    //           return next(e);
    //         });
    //     }
    // })
    //   //return Movie.searchHasStream(query)

  }
  if (mode == 2) {
    return Trailer.search(query)
      .then(movies => res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies }))
      .catch(e => next(e));
  }
  else if (mode == 3) {
    return Movie.searchV2(query)
      .then(movies => {
        movies.forEach(movie => {
          movie.backdrop_path = "";
          movie.poster_path = "";
        });
        return res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies })
      })
      .catch(e => next(e));
  }
  else {
    return Movie.searchV2(query)
      .then(movies => res.json({ page: parseInt(req.query.page), total_pages: -1, total_results: -1, "results": movies }))
      .catch(e => next(e));
  }
}

function getQuality(tag) {
  switch (parseInt(tag)) {
    case 22:
      return 720;
    case 18:
      return 480;
    case 36:
      return 360;
    case 37:
      return 1080;
  }
}

function getVideos(req, res, next) {
  const movieID = req.params.id;
  var mode = 1;
  var versionCode = req.headers.versioncode;
  if (req.headers.mode)
    mode = parseInt(req.headers.mode);
  //console.log("mode =" + mode);
  if (mode == 2) {
    return VideoTrailer.listvideo(movieID)
   
      .then(video => {
        return res.json(video);
      })
      .catch(e => {
        console.log("LOI O DAY 1");
        return next(e)
      }
      );
  }
  else {
    return Video.listvideo(movieID)
      .then(videos => {
        preprocessVideo(videos, movieID);
        if(versionCode < 8) {
          return fillLinkToVideos(mode, videos, movieID, function (newVideos) {
            return res.json(newVideos);
          })
        } else {
          return res.json(videos);
        }
      })
      .catch(e => {
        console.log("LOI O DAY 2");
        return next(e)
      }
      );;
  }
}

function preprocessVideo(videos, id) {
  if (videos == null) {
    console.log("begin import import_trailerVideo");
    Movie.get(id, function (movie) {
      if (movie)
        AdminCtrl.importVideo(movie.tmvdbID, movie.id);
    });

    videos = new Video();
  }
  if (videos.results == null)
    videos.results = [];

  if (videos.results.length > 0) {
    videos.results.forEach(element => {
      element.type = "Trailer";
    });
  }
  else {
    var fakeTrailer = {
      type: "Trailer",
      size: 1080,
      site: "YouTube",
      name: "Official Teaser Trailer",
      key: "xxxx"
    }
    videos.results.push(fakeTrailer);
  }

  return videos;
}

function getLinkFromDuongTang(ggdriveID, next) {
  //checkCache if exist return else request to DuongTang
  var url = `http://104.154.199.96:3003?apikey=cjg9doi6p00051brmfv1taqgt&drive=https://drive.google.com/file/d/${ggdriveID}`;
  return request(url, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      return next(JSON.parse(body));
    }
    else {
      return next(null);
    }
  });
  return next(null);
}

function getLinkLocal(movie, ggLinkID, linkLeng, next) {
  // return LimitViewCount.checkLimit(movie.id,linkLeng,function(canGetLink)
  // {
  if (true) {
    return GGLink.getStreams(ggLinkID, function (gglink) {
      if (gglink && gglink.status) {
        if (gglink && gglink.streams.length > 0) {
          return next(gglink.streams);
        }
        else if (gglink) {
          return ggCtrl.parse3(gglink.url, function (result) {
            if (result.data) {
              return GGLink.createOrUpdate(gglink.email, gglink.url, result.data, function (gglinkObj) {
                return next(result.data);
              });
            }
            else {
              if (result.parseCode == ggCtrl.PARSE_CODE.FAILED) {
                console.log({ "code": 400, "message": "Parse fail", result });
                AdminCtrl.deleteLinkID(gglink.id, true);
              }
              // else if(result.parseCode == ggCtrl.PARSE_CODE.NOT_READY)
              // {
              //   AdminCtrl.saveToMovieNotReady(movie.id,gglink.url);
              // }
              else {
                console.log("parse3 Unknow reason!!");
              }
            }
          });
        }
        else {
          return next(null);
        }
      }
      else {
        AdminCtrl.deleteLinkID(ggLinkID, false);
        if (gglink)
          AdminCtrl.saveToMovieNotReady(movie.id, gglink.url);
        return next(null);
      }
    })
  }
  //});

}
function getLinkFormClient(movie, needToResolver, next) {
  console.log("getLinkFormClient(movie,next) = " + movie.id + " " + movie.title);
  return ClientLink.getLinkByMovieID(movie.id, needToResolver)
    .then(links => {
      return next(links);
    })
    .catch(e => next(null))
}
function getAllLinkFormClient(movie, next) {
  console.log("getAllLinkByMovieID(movie,next) = " + movie.id + " " + movie.title);
  return ClientLink.getAllLinkByMovieID(movie.id)
    .then(links => {
      return next(links);
    })
    .catch(e => next(null))
}
function fillLinkToVideos(mode, videos, movieID, next) {
  return Movie.get(movieID, function (movie) {
    if (!movie) {
      console.log("cant not find the movie = " + movieID);
      return next(videos)
    }
    if (movie.ggLinks && movie.ggLinks.length > 0) {
      var idx = Math.floor((Math.random() * movie.ggLinks.length));
      return getLinkLocal(movie, movie.ggLinks[idx], movie.ggLinks.length, function (streams) {
        if (streams) {
          streams.forEach(function (element) {
            var stream = {
              type: "Stream",
              size: getQuality(element.itag),
              site: element.url,
              name: "STREAM",
              key: ""
            }
            videos.results.push(stream);
          }, this);
        }
        videos._doc.linkID = movie.ggLinks[idx];
        return next(videos);
      })
    }
    else {
      if (movie.tv.id != null && movie.tv.id.length > 0) //Skip for TV
      {
        console.log("Skip for TV");
        return next(videos);
      }

      return getLinkFormClient(movie, false, function (links) {
        if (links && links.length > 0) {
          console.log("links = " + JSON.stringify(links));
          var idx = Math.floor((Math.random() * links.length));
          var stream = {
            type: "Stream",
            size: 720,
            site: links[idx].link,
            name: "STREAM",
            key: ""
          }
          if (videos) {
            videos.results.push(stream);
            if (videos._doc)
              videos._doc.linkID = "CLIENT_LINK" + links[idx]._doc._id;
            else
              videos.linkID = "CLIENT_LINK" + links[idx]._doc._id;
          }
          return next(videos);
        }
        else {
          if (mode != 1) {
            return getLinkFormClient(movie, true, function (link222) {
              if (link222 && link222.length > 0) {
                console.log("link222 = " + JSON.stringify(link222));
                var idx = Math.floor((Math.random() * link222.length));
                var stream = {
                  type: "Stream",
                  size: 720,
                  site: link222[idx].link,
                  name: "STREAM",
                  key: ""
                }
                if (videos) {
                  videos.results.push(stream);
                  if (videos._doc)
                    videos._doc.linkID = "CLIENT_LINK" + link222[idx]._doc._id;
                  else
                    videos.linkID = "CLIENT_LINK" + link222[idx]._doc._id;
                }
                return next(videos);
              }
              else
                return next(videos);
            });
          }
          else {
            return next(videos);
          }
        }
      });
    }
  });
}
function getAllVideo(req, res, next) {
  return Video.list()
    .then(videos => res.json(videos))
    .catch(e => next(e));
}

function getGenreList(req, res, next) {
  return Genre.list()
    .then(genres => res.json({ "genres": genres }))
    .catch(e => next(e));
}

function getTVGenreList(req, res, next) {
  return TVGenre.list()
    .then(genres => res.json({ "genres": genres }))
    .catch(e => next(e));
}




function getReview(req, res, next) {
  var movieID = req.params.id;
  var page = parseInt(req.query.page);

  return Review.getByMovie(movieID)
    .then(review => res.json(review))
    .catch(e => next(e));
}

function reportViewCount(req, res, next) {
  //console.log(JSON.stringify(req.params));
  var movieID = req.params.id;
  return Movie.getLinks(movieID, function (gglinks) {
    var result = [];
    gglinks.forEach(function (element) {
      //console.log(element);
      GGLink.getViewCount(element, function (viewcount) {
        //console.log("viewcount" + viewcount);
        result.push(element + ":" + viewcount);
        if (result.length == gglinks.length) {
          return res.json(result);
        }
      })
    });
  });
}
function getAllReviews(req, res, next) {
  return Review.list()
    .then(reviews => res.json(reviews))
    .catch(e => next(e))
}

function getLinkCount(req, res, next) {
  return GGLink.countAll()
    .then(count => res.json({ count: count }));
}

function getViewPerDay(req, res, next) {
  return Movie.getViewPerDay()
    .then(count => res.json({ viewperday: count }));
}
function getLinkList(req, res, next) {
  return GGLink.listall()
    .then(list => res.json(list))
    .catch(e => next(e));
}

function getStreams(req, res, next) {
  var _id = req.query.gglinkID || 0;
  return GGLink.get(_id)
    .then(gglink => res.json(gglink))
    .catch(e => res.json({ "message": "gglink not found" }))
}

const requestIp = require('request-ip');
function onNewLink(req, res, next) {
  var movieID = parseInt(req.headers.movieid);
  var streamlink = req.headers.streamlink;
  var host = req.headers.hostname;

  if (streamlink) {
    console.log("==============================================================================================================================================");
    console.log("+movieID    :" + movieID);
    console.log("+streamlink :" + streamlink);
    console.log("+host       :" + host);
    console.log("==============================================================================================================================================");
    ClientLink.createOrUpdate(movieID, streamlink, host, false)
      .then(result => res.json(result))
      .catch(e => res.json(result))
  }

}
var url = require('url');
function isNeedToResolver(link) {
  var hostname = url.parse(link).hostname;
  if (hostname.includes("openload")
    || hostname.includes("oload.")
    || hostname.includes("vidlink")
    || hostname.includes("downace")
    || hostname.includes("userscloud")
    || hostname.includes("uptobox")
    || hostname.includes("rapidvideo")
    || hostname.includes("raptu")
    || hostname.includes("streamango")
    || hostname.includes("vidto")
    || hostname.includes("vidlox")
    || hostname.includes("vidtodo")
    || hostname.includes("vodlock")
    || hostname.includes("powvideo")
    || hostname.includes("estream")
    || hostname.includes("daclips")
    || hostname.includes("movpod")
    || hostname.includes("thevideo")
    || hostname.includes("vidzi")
    || hostname.includes("vidoza")
    || hostname.includes("them4ufree")
    || hostname.includes("vidup")
    || hostname.includes("ok")
    || hostname.includes("vidcloud")
    || hostname.includes("vcstream")) {
    return true;
  }
  return false;
}

function isDontNeedToResolver(link) {
  var hostname = url.parse(link).hostname;
  if (hostname.includes("fbcdn")) {
    return true;
  }
  return false;
}
function increaGGLinkViewCount(req, res, next) {
  var linkID = req.headers.linkid;
  var movieID = parseInt(req.headers.movieid);
  var resolver = req.headers.resolver;
  var streamlink = req.headers.streamlink;
  var host = req.headers.host;
  if (streamlink || resolver) {
    console.log("==============================================================================================================================================");
    console.log("movieID    :" + movieID);
    console.log("streamlink :" + streamlink);
    console.log("resolver   :" + resolver);
    console.log("host       :" + host);
    console.log("==============================================================================================================================================");

    Movie.get(movieID, function (movie) {
      if (movie && movie.tv.id == null) {
        if (resolver && resolver.length > 0) {
          ClientLink.createOrUpdate(movieID, resolver, host, true)
            .then(result => {
              console.log("ADD NEED RESOLVER LINK " + resolver)
            })
            .catch(e => {
              console.log("ADD FAIL NEED RESOLVER LINK " + resolver)
            });
        }

        if (streamlink && streamlink.length > 0 && isDontNeedToResolver(streamlink)) {
          ClientLink.createOrUpdate(movieID, streamlink, host, false)
            .then(result => {
              console.log("ADD NEED streamlink LINK " + resolver)
            })
            .catch(e => {
              console.log("ADD FAIL NEED streamlink LINK " + resolver)
            });
        }
      }
    })

  }
  // const clientIp = requestIp.getClientIp(req);
  // LimitViewCount.add(clientIp, movieID);
  return Movie.addViewCount(movieID, function (results) {
    if (results) {
      return GGLink.addViewCount(linkID)
        .then(results => {
          return res.json({ "message": "+1 increaGGLinkViewCount" });
        });
    }
    else
      return res.json({ "message": "increa fail" });
  });
}

function reportLink(req, res, next) {
  var movieID = req.query.movieID;
  var linkID = req.query.linkID;
  var message = req.query.text;
  var videoID = req.query.videoID;
  console.log(`report linkID = ${linkID} message = ${message} mvID = ${movieID}`);
  Report.set(movieID, linkID, function (report) {
    return res.json(report);
  })
}

function notifyLinkdie(req, res, next) {
  var movieID = req.query.movieID;
  var linkID = req.query.linkID;
  var message = req.query.message;
  var videoID = req.body.videoID;
  var errorCode = parseInt(message);
  if (linkID.includes("SERVER_LINK")) {

  }
  else if (linkID.includes("CLIENT_LINK")) {
    videoID = linkID.replace("CLIENT_LINK", "");
    console.log("=====Remove Client Link = " + videoID)
    if (videoID.match(/^[0-9a-fA-F]{24}$/) && (errorCode == 403 || errorCode == 404 || errorCode == 503 || errorCode == 429)) {
      console.log("=====Remove Client Link2 = " + videoID)
      return ClientLink.get(videoID)
        .then(clientLink => {

          console.log("=====Remove Client Link20 = " + clientLink)

          if (clientLink && clientLink.link) {
            ggCtrl.checkLinkDie(clientLink.link, function (isDied) {

              console.log("=====Remove Client Link3 = " + videoID + " isDied = " + isDied);
              if (isDied) {
                ClientLink.removebyID(videoID);
                return res.json({ code: isDied })
              } else {
                return res.json({ code: "unknow" })
              }
            })
          }
          else {
            return res.json({ code: "not exist!" });
          }
        })
    }
  }
  else if (linkID && (errorCode == 403 || errorCode == 404 || errorCode == 503)) {
    return GGLink.getStreams(linkID, function (link) {
      if (link && link.streams && link.streams.length > 0) {
        console.log(link.streams[0])
        ggCtrl.checkLinkDie(link.streams[0].url, function (isDied) {
          if (isDied) {
            AdminCtrl.deleteLinkID(linkID, true);
            return res.json({ code: isDied })
          } else {
            return res.json({ code: "unknow" })
          }
        })
      }
      else
        return res.json({ code: "unknow" })
    });
  }
  else {
    // console.log("notifyLinkdie req.query.movieID" + req.query.movieID)
    // console.log("notifyLinkdie req.query.linkID" + req.query.linkID)
    // console.log("notifyLinkdie req.query.message" + req.query.message)
    // console.log("notifyLinkdie req.query.videoID" + req.query.videoID)

    if (errorCode == 429) {
      return GGLink.updateStatus(linkID, false, function (link) {
        //console.log("link = " + link);
        return res.json({ code: "unknow" })
      });
    }

    return res.json({ code: "unknow" })
  }
}

function increaViewCount(req, res, next) {
  var movieID = req.query.movieID;
  return Movie.addViewCount(movieID, function (results) {
    if (results)
      return res.json({ "message": "+1 increaViewCount" });
    else
      return res.json({ "message": "increa fail" });
  });
}

function increaRequest(req, res, next) {
  var movieID = req.query.movieID;
  Movie.addRequest(movieID);
  return res.json({ "message": "+1 increaRequest" });
}

function getSeries(req, res, next) {
  var movieID = req.query.movieID;
  var tvID = req.query.tvID;
  var sessionID = req.query.sessionID;
  return Session.getByTv(tvID, sessionID)
    .then(result => {
      if (result)
        return res.json(result);
      else
        return res.json([]);
    })
}
function getSession(req, res, next) {
  var tvID = req.query.tvID;
  var mode = 1;
  if (req.headers.mode)
    mode = parseInt(req.headers.mode);

  if (mode == 3) {
    return Session.get(tvID)
      .then(result => {
        result.forEach(element => {
          element.poster_path = "";
        });
        return res.json({ sessions: result });
      })
  }
  else {
    return Session.get(tvID)
      .then(result => {
        return res.json({ sessions: result });
      })
  }
}

function movieDetail(req, res, next) {
  var movieID = req.query.id;
  return Movie.get(movieID, function (movie) {
    return res.json(movie);
  })
}

function getListClientLink(req, res, next) {
  return ClientLink.getlist()
    .then(list => res.json(list))
    .catch(e => res.json({ message: "NOT LINK" }))
}


export default {
  create
  , update
  , list
  , listtv
  , listbyGenre
  , listTVbyGenre
  , listHasLink
  , search
  , getVideos
  , getStreams
  , getAllVideo
  , getReview
  , reportViewCount
  , getGenreList
  , getTVGenreList
  , getLinkList
  , getAllReviews
  , getLinkCount
  , getViewPerDay
  , increaGGLinkViewCount
  , onNewLink
  , reportLink
  , notifyLinkdie
  , increaViewCount
  , increaRequest
  , getSeries
  , movieDetail
  , getListClientLink
  , getSession
};
