var base64 = require('base-64');
var redis = require("redis");
var redisClient = redis.createClient();
const requestIp = require('request-ip');
import ConfigCtrl from './config.controller';
const PRE_FIX = "CHECK_API_KEY_FAILED";
const TTL = 60 * 60 * 24 * 7;

function check_api_key(req, res, next) {
    var api_key = req.headers.api_key;
    var versioncode = req.headers.versioncode;

    const clientIp = requestIp.getClientIp(req);
    //console.log("=============================clientIp" + clientIp);


    var packageName = getPackageName(api_key, req);

    // if(!req.headers.mode)
    //     console.log("=============" + clientIp+ "======================= check_api_key req.headers.mode = " + req.headers.mode + "  packageName = " + packageName);

    return ConfigCtrl.hasConfig(packageName, versioncode, function (hasConfig) {
        if (hasConfig)
            return next();
        else {
            console.log("check_api_key failed " + api_key + "version code = " + versioncode);
            return res.json({ "message": "api_key is wrong" });
        }
    })
}

function getPackageName(api_key, req) {
    if (api_key && api_key.length > 20) {
        var base64String = api_key.substring(20);
        var packageName = base64.decode(base64String)
        return packageName;
    }
    else {
        var ip = requestIp.getClientIp(req);
        var key = PRE_FIX + ip;
        redisClient.get(key, function (reply) {
            if (reply) {
                console.log("redisClient.incr(key,function(reply){});");
                redisClient.incr(key, function (abc) { });
            }
            else {
                redisClient.set(key, 1, function (err, reply2) {
                    if (err == null)
                        redisClient.expire(key, TTL, redis.print);
                });
            }
        });
    }
    return "";
}

export default { check_api_key, getPackageName };
