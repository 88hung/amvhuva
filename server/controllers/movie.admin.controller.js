const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.Types.ObjectId;

import Movie from '../models/movie.model';
import Review from '../models/review.model';
import Genre from '../models/genre.model';
import TVGenre from '../models/tv_genre.model';

import Video from '../models/video.model';
import GGLink from '../models/gglink.model';
import Configs from '../models/config.model';
import Series from '../models/series.model';
import Session from '../models/session.model';
import Trailer from '../models/trailer.model';
import VideoTrailer from '../models/videoTrailer.model';
import ggCtrl from '../controllers/googlePhotoParser.controller';
import GGSheetCtrl from '../controllers/importGGSheet';
import MovieNotReady from '../models/movieNotReady.model';

import path from 'path'

const http = require('https');
const http2 = require('http');

function search(req, res, next) {
  const query = { limit: 20, skip: 0, query: req.query.query };
  Movie.searchByTitle(query)
    .then((movies) => {
      if (movies.length > 0) { res.json(movies); } else {
        searchFromAnotherDB(req, res, next);
      }
    })
    .catch((e) => {
      next(e);// searchFromAnotherDB(req,res,next);
    });
}

function updateOrCreateGGLink(req, res, next) {
  var movieID = req.body.movieID;
  var url = req.body.gglink;
  var code = "79468658" || req.query.code;
  var email = req.body.email;
  var subtitle = req.body.subtitle;
  //check has movie 
  Movie.get(movieID, function (movie) {
    if (movie) {
      ggCtrl.parse2(req, res, function (result) {
        if (result.data) {
          GGLink.createOrUpdate(email, url, result.data, function (gglinkObj) {
            if (movie.ggLinks.indexOf(gglinkObj._id) < 0) {
              movie.ggLinks.push(gglinkObj._id);
              movie.subtitle_identify = subtitle;
              movie.save();
              return res.json({ "code": 200, "message": "parse + save success", result });
            }
            return res.json({ "code": 300, "message": "parse succes + save fail", result });
          });
        }
        else
          return res.json({ "code": 400, "message": "Parse fail", result });
      });
    }
    else return res.json({ "code": 500, "message": "Movie not exist, please try again!" })
  });
  //
}


function updateSubtile(req, res, next) {
  var movieID = req.body.movieID;
  var subtile = req.body.subtile;
  Movie.updateSubtile(movieID, subtile, function (result) {
    if (result) {
      return res.json({ "code": 200, message: "update Success!" });
    }
    else {
      return res.json({ "code": 404, message: "Update fail!" });
    }
  });
}

function resetViewPerDay(req, res, next) {
  var limit = parseInt(req.query.limit) || 3000
  Movie.resetCountPerDaylist(limit);
  return res.json({ message: "Done" });
}

function resetRequestCount(req, res, next) {
  var count = parseInt(req.headers.count);
  console.log("resetRequestCount = " + count)
  Movie.resetRequestCount(count);
  return res.json({ message: "done" });
}

function getAllLink429(req, res, next) {
  return GGLink.listStatusFalse()
    .then(list => res.json({ count: list.length, list: list }))
    .catch(e => next(e));
}

function getHeapDump(req, res, next) {
  var heapdump = require('heapdump');
  var filename = path.join(__dirname, "../heapdump/" + Date.now() + '.heapsnapshot');
  console.log("filename = " + filename);
  heapdump.writeSnapshot(filename);
  return res.json({ message: "done!" });
}

function deletelink(req, res, next) {
  var gglink = req.body.gglink;
  GGLink.removeLink(gglink, function (id, link, mail, viewcount) {
    Movie.removeLinkByLinkID(id, function (movie) {
      Session.removeSeriesByID(id, function (result) {
        if (movie) {
          var obj = { id: movie.id, name: movie.title, retain: movie.ggLinks.length, link: link, mail: mail, viewcount: viewcount };
          sendDeletedLinkToSlack(obj);
        }
        return res.json({ message: "remove in session success!", result });
      })
    });
  })

}

function deleteLinkID(id, isSendToSlack) {
  var gglinkID = id;
  GGLink.removeLinkByID(gglinkID, function (id, link, mail, viewcount) {
    Movie.removeLinkByLinkID(gglinkID, function (movie) {
      Session.removeSeriesByID(gglinkID, function (result) {
        if (movie) {
          var obj = { id: movie.tmvdbID, name: movie.title, retain: movie.ggLinks.length, link: link, mail: mail, viewcount: viewcount };
          if (isSendToSlack)
            sendDeletedLinkToSlack(obj);
        }
      })
    });
  })
}

function saveToMovieNotReady(id, gglink) {
  return MovieNotReady.createOrUpdate(id, gglink, function (result) {
    console.log("saveToMovieNotReady id = " + id + " | gglink = " + gglink);
    return result;
  });
}

function deleteLinkByID(req, res, next) {
  deleteLinkID(req.body.gglinkID, true)
  return res.json({ message: "remove  success!" });
}


function sendDeletedLinkToSlack(obj) {

  console.log("send to slack " + obj.link);
  console.log("send to movieID " + obj.id);
  console.log("send to mail " + obj.mail);
  console.log("send to title " + obj.name);
  console.log("send to retain " + obj.retain);

  var url = "https://slack.com/api/chat.postMessage?token=xoxp-242080693313-261025019845-267157917989-839784e5b2fcf9dd569db1fc34a23211&channel=reportmv&text="
    + obj.mail
    + "," + obj.link
    + "," + obj.id
    + ",viewcount:" + obj.viewcount
    + ",name:" + obj.name
    + ",retain:" + obj.retain
    + "&pretty=1";

  http.get(url, (resp) => {
    let data = '';
    resp.on('data', (chunk) => {
      data += chunk;
    });
    resp.on('end', () => {
      console.log("Sent to slack link " + obj.link);
    });

  }).on("error", (err) => {
    console.log("Error: " + err.message);
  });
}
function updateAdult(req, res, next) {
  var id = req.body.movieID;
  var isAdult = req.body.isAdult;
  return Movie.updateAdult(id, isAdult, function (result) {
    return res.json(result);
  })
}

function updateDriveLinks(req, res, next) {
  var movieID = req.body.tmvdbID;
  var driveID = req.body.driveID;
  return Movie.addDriveLink(movieID, driveID, function (result) {
    return res.json(result);
  })
}

function removeDriveLink(req, res, next) {
  var driveID = req.body.driveID;
  return Movie.removeDriveLink(driveID, function (results) {
    return res.json(results);
  })
}

function updateMovieNotReady(req, res, next) {
  return MovieNotReady.list()
    .then(movies => {
      movies.forEach(element => {
        var abc = { id: element.movieID, url: element.GGLink, email: "unknow", subtitle: "" };
        var deltaTime = new Date() - element.createdAt;

        if ((new Date() - element.createdAt) / 1000 / 60 / 60 / 24 >= 1) {
          element.remove();
          updateGGLink(abc);
        }
      });
      return res.json(movies);
    })
    .catch(e => res.join({ messag: "null" }));
}

function updateMovieNotReady2() {
  return MovieNotReady.list()
    .then(movies => {
      movies.forEach(element => {
        var abc = { id: element.movieID, url: element.GGLink, email: "unknow", subtitle: "" };
        var deltaTime = new Date() - element.createdAt;

        if ((new Date() - element.createdAt) / 1000 / 60 / 60 / 24 >= 1) {
          element.remove();
          updateGGLink(abc);
        }
      });
      return null;
    })
    .catch(e => null)
}

function updateGGLink(abc) {
  var id = abc.id;
  var gglink = abc.url;
  var newEmail = abc.email;
  var subtitle = abc.subtitle;
  return Movie.get(id, function (movie) {
    console.log('Begin insert for  tmvhdid : %d , email = %s , url = %s', id, newEmail, gglink);
    if (movie) {
      return GGLink.createOrUpdate(newEmail, gglink, [], function (gglinkObj) {
        if (gglinkObj) {
          if (movie.ggLinks.indexOf(gglinkObj._id) < 0) {
            movie.ggLinks.push(gglinkObj._id);
            movie.requestCount = 0;
            movie.save();
            console.log({ "code": 200, "message": "create new save GGLINK success" });
          }
        }
        else {
          console.log({ "code": 600, "message": "update gglink fail, link had already exists" });
        }
        return null;
      });
    }
    else console.log({ "code": 500, "message": "Movie not exist, please try again!" })
  });
}
function updateTV(req, res, next) {
  var url = req.body.url;
  var email = req.body.email;
  var ep = req.body.ep;
  var subtitle = req.body.subtitle;
  var tvID = req.body.tvID;
  var sessionID = req.body.sessionID;
  console.log(JSON.stringify(req.body));

  GGLink.getByUrl(url)
    .then(gglink => {
      if (gglink) { //if exist
        var linkID = gglink._id;
        var chap = { ep: ep, ggLinks: [linkID], subtitle: subtitle };
        saveToSession(tvID, sessionID, chap, function (result) {
          return res.json(result);
        })
      }
      else //not exist 
      {
        ggCtrl.parse3(url, function (result) {
          if (result.data) {
            console.log(result.data);
            GGLink.createOrUpdate(email, url, result.data, function (gglinkObj) {
              var chap = { ep: ep, ggLinks: [gglinkObj._id], subtitle: subtitle };
              saveToSession(tvID, sessionID, chap, function (result) {
                return res.json(result);
              })
            });
          }
          else {
            return res.json({ "code": 400, "message": "Parse fail", result });
          }
        })
      }
    })
}
function saveToSession(tvID, sessionID, chap, next) {
  console.log("tvID" + tvID);
  console.log("sessionID" + sessionID);
  console.log(chap);
  var chapRes = chap
  Session.getByTv(tvID, sessionID)
    .then(session => {
      console.log("session = " + JSON.stringify(session));
      if (session) {
        var temp = null; //check exist chap
        session.series.forEach(function (item) {
          if (item.ep == chapRes.ep) {
            temp = item;
            return false;
          }
          return true;
        }, this);

        if (temp) {
          if (temp.ggLinks.indexOf(chapRes.ggLinks[0]) < 0)
            temp.ggLinks.push(chapRes.ggLinks[0]);
        }
        else {
          var newchap = { ep: chapRes.ep, ggLinks: chapRes.ggLinks, subtitle: chapRes.subtitle };
          session.series.push(newchap);
        }
        session.save();
        Movie.setActiveTV(tvID, true);
        return next(session);
      }
      else {
        var newSeries = new Session();
        newSeries.tvID = tvID;
        var newChap = { ep: chapRes.ep, ggLinks: chapRes.ggLinks, subtitle: chapRes.subtitle };
        newSeries.series.push(newChap);
        newSeries.save();
        Movie.setActiveTV(tvID, true);
        return next(newSeries);
      }
    })
    .catch(e => next(e));
}

function searchFromAnotherDB(req, res, next) {
  console.log('searchFromAnotherDB========================================');
  const options = {
    method: 'GET',
    hostname: 'api.themoviedb.org',
    port: null,
    path: `/3/search/movie?include_adult=false&page=1&query=${req.query.query.replace(' ', '%20')}&language=en-US&api_key=a07e22bc18f5cb106bfe4cc1f83ad8ed`,
    headers: {}
  };

  var req = http.request(options, (respose) => {
    const chunks = [];

    respose.on('data', (chunk) => {
      chunks.push(chunk);
    });

    respose.on('end', () => {
      const body = Buffer.concat(chunks);
      res.json(body);
    });
  });

  req.write('{}');
  req.end();
}

function importTrailer(req, res, next) {
  const import_func = function (page) {
    const options = {
      protocol: 'http:',
      method: 'GET',
      host: '52.170.213.18',
      port: 3002,
      path: `/discover/movie?page=${page}`,
      headers: {
        "api_key": "MWFzZDQzNDNTRFNkQEAjY29tLmdseC5oZG1vdmllcw==",
        "versioncode": 0
      }
    };

    const req = http2.request(options, (res2) => {
      const chunks = [];

      res2.on('data', (chunk) => {
        chunks.push(chunk);
      });

      res2.on('end', function () {
        const buffer = Buffer.concat(chunks);
        console.log(buffer.toString());

        const listMovies = JSON.parse(buffer.toString());
        listMovies.results.forEach((element) => {
          Trailer.set(element);
        }, this);

        console.log(`${page} complete===============================================================================${listMovies.total_pages}`);

        if (listMovies.total_pages >= page) {
          page += 1;
          console.log(`${page} start===============================================================================`);
          setTimeout(() => {
            import_func(page);
          }, 1500);
        }
        else {
          return res({ message: "complete" });
        }
      });
    });

    req.write('{}');
    req.end();
  }
  import_func(1);
}

function import_trailerVideo(movieID) {
  const options = {
    protocol: 'http:',
    method: 'GET',
    host: '52.170.213.18',
    port: 3001,
    path: `/movie/${movieID}/videos`,
    headers: { "api_key": "01234567890123456789Y29tLmh1dmEuZnJlZW1vdmllcw==" }
  };

  const req = http2.request(options, (res) => {
    const chunks = [];

    res.on('data', (chunk) => {
      chunks.push(chunk);
    });

    res.on('end', () => {
      const buffer = Buffer.concat(chunks);

      var data = JSON.parse(buffer.toString());

      console.log(data.results);
      VideoTrailer.createOrUpdate(movieID, data.results)
    });
  });

  req.write('{}');
  req.end();
};

function importVideoTrailer(req2, res2, next) {

  Trailer.find().exec((err, data) => {
    let i = 0;
    let total = data.length;
    data.forEach((element) => {
      setTimeout(() => {
        total--;
        console.log(` begin import video ${element.id} has left ${total}`);
        import_trailerVideo(element.id);
      }, i * 1200);
      i++;
    });
    return res2.json("complete");
  });
}


function importDataBase(req, res, next) {
  const import_func = function (page) {
    const options = {
      method: 'GET',
      hostname: 'api.themoviedb.org',
      port: null,
      path: `/3/discover/movie?page=1&include_video=false&include_adult=false&sort_by=popularity.desc&language=en-US&api_key=a07e22bc18f5cb106bfe4cc1f83ad8ed&page=${page}`,
      headers: {}
    };

    const req = http.request(options, (res2) => {
      const chunks = [];

      res2.on('data', (chunk) => {
        chunks.push(chunk);
      });

      res2.on('end', function () {
        const buffer = Buffer.concat(chunks);
        console.log(buffer.toString());

        const listMovies = JSON.parse(buffer.toString());
        listMovies.results.forEach((element) => {
          element.tmvdbID = element.id;

          var releaseY = parseInt(element.release_date.split("-")[0]);
          var year = (new Date()).getFullYear();

          if (releaseY <= year && element.poster_path && element.backdrop_path)
            Movie.setFromTMVDB(element);
        }, this);

        console.log(`${page} complete===============================================================================${listMovies.total_pages}`);

        if (listMovies.total_pages >= page) {
          page += 1;
          console.log(`${page} start===============================================================================`);
          setTimeout(() => {
            import_func(page);
          }, 1500);
        }
        else {
          return res({ message: "complete" });
        }
      });
    });

    req.write('{}');
    req.end();
  };

  import_func(1);
}

function importTVGenre(req, res2, next) {
  const options = {
    method: 'GET',
    hostname: 'api.themoviedb.org',
    port: null,
    path: '/3/genre/tv/list?language=en-US&api_key=a07e22bc18f5cb106bfe4cc1f83ad8ed',
    headers: {}
  };

  var req = http.request(options, (res) => {
    const chunks = [];

    res.on('data', (chunk) => {
      chunks.push(chunk);
    });

    res.on('end', function () {
      const buffer = Buffer.concat(chunks);
      console.log(buffer.toString());
      const genres = JSON.parse(buffer.toString());
      genres.genres.forEach((element) => {
        TVGenre.findOne({ id: element.id }).exec((err, data) => {
          if (data == null) {
            var genre = new TVGenre({ id: element.id, name: element.name });
            genre.save();
          } else {
            var genre = data;
            genre.name = element.name;
            genre.save();
          }
        });
      }, this);
      return res2.json({ message: 'save success!' });
    });
  });

  req.write('{}');
  req.end();
}

function importGenre(req, res2, next) {
  const options = {
    method: 'GET',
    hostname: 'api.themoviedb.org',
    port: null,
    path: '/3/genre/movie/list?language=en-US&api_key=a07e22bc18f5cb106bfe4cc1f83ad8ed',
    headers: {}
  };

  var req = http.request(options, (res) => {
    const chunks = [];

    res.on('data', (chunk) => {
      chunks.push(chunk);
    });

    res.on('end', function () {
      const buffer = Buffer.concat(chunks);
      console.log(buffer.toString());
      const genres = JSON.parse(buffer.toString());
      genres.genres.forEach((element) => {
        Genre.findOne({ id: element.id }).exec((err, data) => {
          if (data == null) {
            var genre = new Genre({ id: element.id, name: element.name });
            genre.save();
          } else {
            var genre = data;
            genre.name = element.name;
            genre.save();
          }
        });
      }, this);
      return res2.json({ message: 'save success!' });
    });
  });

  req.write('{}');
  req.end();
}

//nho sua 2 ham hay theo tmvdbID
function importReview(req, res2, next) {
  const import_func = function (movieID) {
    const options = {
      method: 'GET',
      hostname: 'api.themoviedb.org',
      port: null,
      path: `/3/movie/${movieID}/reviews?page=1&language=en-US&api_key=a07e22bc18f5cb106bfe4cc1f83ad8ed`,
      headers: {}
    };

    const req = http.request(options, (res) => {
      const chunks = [];
      res.on('data', (chunk) => {
        chunks.push(chunk);
      });

      res.on('end', () => {
        const body = Buffer.concat(chunks);
        console.log(body.toString());

        Review.find({ id: movieID }).exec((err, data) => {
          if (err == null && data != null) {
            var review = new Review();
            review.results = (JSON.parse(body.toString())).results;
            review.save();
          }
        });
        //return res2.json({ message: 'save success!' });
      });
    });

    req.write('{}');
    req.end();
  };

  Movie.find().exec((err, data) => {
    let i = 0;
    let total = data.length;
    data.forEach((element) => {
      setTimeout(() => {
        total--;
        console.log(` begin import review ${element.tmvdbID} has left ${total}`);
        import_func(element.tmvdbID);
      }, i * 1000);
      i++;
    });
  });
}

function importVideo(tmvdbID, movieID) {
  const options = {
    method: 'GET',
    hostname: 'api.themoviedb.org',
    port: null,
    path: `/3/movie/${tmvdbID}/videos?api_key=a07e22bc18f5cb106bfe4cc1f83ad8ed&language=en-US`,
    headers: {}
  };

  const req = http.request(options, (res) => {
    const chunks = [];

    res.on('data', (chunk) => {
      chunks.push(chunk);
    });

    res.on('end', () => {
      const buffer = Buffer.concat(chunks);
      console.log(buffer.toString());
      var data = null;
      try {
        data = JSON.parse(buffer.toString());
      } catch (e) {
        return console.error(e);
      }
      if (data && data.results && data.results.length > 0)
        return Video.createOrUpdate(movieID, data.results)
      else {
        var newVideos = [];
        var stream = {
          type: "Trailer",
          size: 1080,
          site: "YouTube",
          name: "Official Teaser Trailer",
          key: "xxxx"
        }
        newVideos.push(stream);
        return Video.createOrUpdate(movieID, newVideos)
      }
    });
  });

  req.write('{}');
  req.end();
};
function importVideos(req2, res2, next) {
  return Movie.find().exec((err, data) => {
    let i = 0;
    let total = data.length;
    data.forEach((element) => {
      setTimeout(() => {
        total--;
        console.log(` begin import video ${element.tmvdbID} has left ${total}`);
        importVideo(element.tmvdbID, element.id);
      }, i * 1200);
      i++;
    });
    return res2.json("complete");
  });
}

function importFromGGSheet(req, res, next) {
  return GGSheetCtrl.importFromGGSheet(req, res, next);
}

function exportMovieCSV(req, res, next) {
  return GGSheetCtrl.exportCSV(req, res, next);
}


function exportMovieCSVRequestCount(req, res, next) {
  return GGSheetCtrl.exportCSVRequestCount(req, res, next);
}

function exportNoSubtitleCSV(req, res, next) {
  return GGSheetCtrl.exportNoSubtitleCSV(req, res, next);
}

function importTVSchedule() {
  var maxpage = 25;
  var startPage = 1;
  getData(startPage);
  function getData(page) {
    if (page > maxpage)
      return resbase.json("done");

    console.log("======Start=====getData = " + page);
    var http = require("https");

    var options = {
      "method": "GET",
      "hostname": "api.themoviedb.org",
      "port": null,
      "path": "/3/tv/popular?page=" + page + "&language=en-US&api_key=a07e22bc18f5cb106bfe4cc1f83ad8ed",
      "headers": {}
    };

    var req = http.request(options, function (res) {
      var chunks = [];

      res.on("data", function (chunk) {
        chunks.push(chunk);
      });

      res.on("end", function () {
        var body = Buffer.concat(chunks);
        var listMovies = null;
        try {
          listMovies = JSON.parse(body.toString());
        } catch (e) {
          console.log(e);
        };
        if (listMovies != null) {
          var i = 0;
          listMovies.results.forEach((element) => {
            element.tv = {};
            element.tv.id = element.id;
            element.title = element.name;
            element.original_title = element.original_name;
            //console.log(element);
            element.release_date = element.first_air_date;
            var releaseY = parseInt(element.first_air_date.split("-")[0]);
            var year = (new Date()).getFullYear();

            if (releaseY <= year && element.poster_path && element.backdrop_path) {
              //console.log(element);
              setTimeout(function () {
                getDetail(element)
              }, i * 2000);
              i++;
              function getDetail(element) {
                Movie.setFromTMVDBTV(element, function (movie) {
                  var http = require("https");

                  var options = {
                    "method": "GET",
                    "hostname": "api.themoviedb.org",
                    "port": null,
                    "path": "/3/tv/" + movie.tv.id + "?language=en-US&api_key=a07e22bc18f5cb106bfe4cc1f83ad8ed",
                    "headers": {}
                  };

                  var req = http.request(options, function (res) {
                    var chunks = [];

                    res.on("data", function (chunk) {
                      chunks.push(chunk);
                    });

                    res.on("end", function () {
                      var body = Buffer.concat(chunks);

                      var tv_detail = null;
                      try {
                        tv_detail = JSON.parse(body.toString());
                      } catch (e) {
                        console.log(e);
                      };
                      if (tv_detail != null) {
                        movie.tv.sessions = [];
                        //console.log(tv_detail)
                        tv_detail.seasons.forEach((element) => {
                          if (movie.tv.sessions.indexOf(element) < 0)
                            movie.tv.sessions.push(element.id);
                          element.tvID = movie.tv.id;
                          element.movieID = movie.id;
                          Session.createOrUpdate(element, function (update) {
                            //console.log(update);
                          });
                        });
                        console.log("tv ID = " + movie.tv.id)
                        movie.save();
                      }
                    });
                  });

                  req.write("{}");
                  req.end();
                });
              }
            }

          });


          setTimeout(function () {
            getData(startPage);
          }, listMovies.results.length * 2000);
          startPage++;
        }
      });
    });

    req.write("{}");
    req.end();
  }
}
function importTVDatabase(req, resbase, next) {
  importTVSchedule();
}


export default {
  search
  , searchFromAnotherDB
  , importDataBase
  , importTrailer
  , importVideoTrailer
  , resetViewPerDay
  , getHeapDump
  , import_trailerVideo
  , importTVDatabase
  , importFromGGSheet
  , exportMovieCSV
  , exportMovieCSVRequestCount
  , exportNoSubtitleCSV
  , importGenre
  , importTVGenre
  , importReview
  , importVideo
  , importVideos
  , updateOrCreateGGLink
  , deletelink
  , deleteLinkID
  , deleteLinkByID
  , updateSubtile
  , saveToMovieNotReady
  , updateAdult
  , resetRequestCount
  , getAllLink429
  , updateDriveLinks
  , removeDriveLink
  , updateMovieNotReady
  , updateMovieNotReady2
  , importTVSchedule
  , updateTV
};
