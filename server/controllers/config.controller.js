import Configs from '../models/config.model';
import TokenMgr from './token.controller'
import { isNumber } from 'util';
var ipgeoblock = require("node-ipgeoblock");
var async = require('async');
var geoip = require('geoip-lite');
const requestIp = require('request-ip');

var redis = require("redis");
var schedQueueClient = redis.createClient();

schedQueueClient.on('error', function (err) {
  console.log('error event - ' + schedQueueClient.host + ':' + schedQueueClient.port + ' - ' + err);
});
const LIMIT_PUSH = 3;
function denyIP(country) {

}
function getConfig(req, res, next) {
  var api_key = req.headers.api_key;
  var packageName = TokenMgr.getPackageName(api_key, req);
  var versionCode = req.headers.versioncode;
  console.log("packageName=" + packageName + " & versionCode=" + versionCode);
  Configs.get(packageName, versionCode, function (config) {
    if (config) {
      console.log("======" + packageName + "=======" + versionCode + "======================= getConfig config.mode = " + config.configs.mode);
      var geo = geoip.lookup(geoip.pretty(requestIp.getClientIp(req)));
      if (geo === 'VN') {
        console.log("======geo=" + geo);
        return res.json("haven't config yet!");
      }
      else {
        checkConfig(versionCode, packageName, function (isLimit) {
          if (isLimit) {
            config.configs.force.package_name = "";
            config.configs.force.external_link = "";
          }
          return res.json(config.configs);
        })
      }
    }
    else {
      console.log("======" + packageName + "=======" + versionCode + "======================= getConfig FAILED");
      return res.json("haven't config yet!");
    }
  })
}
function hasConfig(packageName, versionCode, next) {
  return Configs.get(packageName, versionCode, function (configs) {
    if (configs)
      return next(true);
    return next(false);
  })
}
const PUSH_TTL = 1000 * 60 * 60 * 24;
function checkConfig(versionCode, packageName, next) {
  //if time bettwen 0~3h return true; 
  var date = new Date();
  var current_hour = date.getHours();
  console.log("current_hour = " + current_hour);

  var key = "PUSH_" + versionCode + "_" + packageName;
  var limitkey = "LIMIT_PUSH_" + versionCode + "_" + packageName;

  if (!isNaN(limitkey))
    return true;

  schedQueueClient.get(limitkey, function (err, limitReply) {
    if (limitReply) {
      schedQueueClient.get(key, function (err, pushNumber) {
        console.log("pushNumber = " + pushNumber);
        if (pushNumber) {
          if (parseInt(pushNumber) < parseInt(limitReply)) {
            schedQueueClient.incr(key, function (err, value) {
              console.log("value = " + value);
              console.log("limitReply = " + limitReply);
              return next(false);
            });
          }
          else {
            return next(true);
          }
        }
        else {
          console.log("key = " + key);
          schedQueueClient.set(key, 1, function (err, reply) {
            if (err == null) {
              schedQueueClient.expire(key, PUSH_TTL, function (err, reply2) {
                return next(1 < parseInt(limitReply));
              })
            }
            else
              return next(true);
          })
        }
      })
    }
    else {
      console.log("LIMIT PUSH NOT SET" + packageName);
      return next(true);
    }
  })
}
function createOrUpdateConfig(req, res, next) {
  var packageName = req.body.packageName;
  var versionCode = req.body.versionCode;
  var configs = req.body.configs;

  Configs.set(packageName, versionCode, configs, function (config) {
    return res.json(config);
  })
}

function getallconfigs(req, res, next) {
  Configs.list(function (config) {
    return res.json(config);
  })
}

function checkNewVersion(req, res, next) {

}
function updateLimitPush(req, res, next) {
  var limitPush = parseInt(req.body.limitPush);
  var versionCode = req.body.versionCode;
  var packageName = req.body.packageName;

  var key = "LIMIT_PUSH_" + versionCode + "_" + packageName;
  return schedQueueClient.set(key, limitPush, function (err, reply) {

    var pattern = "*" + versionCode + "_" + packageName;
    schedQueueClient.keys(pattern, function (err, keys) {

      if (err) return res.json(err);
      if (keys) {
        return async.map(keys, function (key, cb) {
          schedQueueClient.get(key, function (error, value) {
            if (error)
              return cb(error);
            var job = {};
            job['key'] = key;
            job['value'] = value;
            cb(null, job);
          });
        }, function (error, results) {
          if (error)
            return res.json(error);
          //console.log(results);
          results.sort(function (a, b) {
            var countA = parseInt(a.count);
            var countB = parseInt(b.count);
            if (countA < countB)
              return 1;
            if (countA > countB)
              return -1;
            return 0;
          });
          results.err = err;
          results.reply = reply;
          return res.json({ results: results, err: err, reply: reply });
        });
      }
    });
  });
}

function deleteKey(pattern) {
  schedQueueClient.keys(pattern, function (err, keys) {
    if (err) return;

    if (keys) {
      return async.map(keys, function (key, cb) {
        schedQueueClient.del(key, redis.print);
      }, function (error, results) {
        console.log("delete error" + error);
      });
    }
  });
}

export default {
  createOrUpdateConfig
  , getConfig
  , getallconfigs
  , checkNewVersion
  , updateLimitPush
  , deleteKey
  , hasConfig
};
