const fs = require('fs');
const readline = require('readline');
const google = require('googleapis');
const googleAuth = require('google-auth-library');
import Movie from '../models/movie.model';
import Review from '../models/review.model';
import Genre from '../models/genre.model';
import Video from '../models/video.model';
import GGLink from '../models/gglink.model';
// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/sheets.googleapis.com-nodejs-quickstart.json
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly'];
//const TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE) + "/.credentials/";
var TOKEN_DIR = 'C:/Users/HUNGHOANG/.credentials/';
const TOKEN_PATH = TOKEN_DIR + "sheets.googleapis.com-nodejs-quickstart.json";



function importFromGGSheet(req, res, next) {
  var sheetName = req.query.sheetName;
  var isSubOnly = 0;
  if (req.query.subOnly)
    isSubOnly = parseInt(req.query.subOnly);

  // Load client secrets from a local file.
  fs.readFile('client_secret.json', function processClientSecrets(err, content) {
    if (err) {
      res.json({ "message": 'Error loading client secret file: ' + err });
      return;
    }
    // Authorize a client with the loaded credentials, then call the
    // Google Sheets API.
    authorize(JSON.parse(content), listMajors);
  });

  /**
   * Create an OAuth2 client with the given credentials, and then execute the
   * given callback function.
   *
   * @param {Object} credentials The authorization client credentials.
   * @param {function} callback The callback to call with the authorized client.
   */
  function authorize(credentials, callback) {
    var clientSecret = credentials.installed.client_secret;
    var clientId = credentials.installed.client_id;
    var redirectUrl = credentials.installed.redirect_uris[0];
    var auth = new googleAuth();
    var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, function (err, token) {
      if (err) {
        getNewToken(oauth2Client, callback);
      } else {
        oauth2Client.credentials = JSON.parse(token);
        callback(oauth2Client);
      }
    });
  }

  /**
   * Get and store new token after prompting for user authorization, and then
   * execute the given callback with the authorized OAuth2 client.
   *
   * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
   * @param {getEventsCallback} callback The callback to call with the authorized
   *     client.
   */
  function getNewToken(oauth2Client, callback) {
    var authUrl = oauth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES
    });
    res.json({ "message": 'Authorize this app by visiting this url: ' + authUrl });
    var rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });
    // rl.question('Enter the code from that page here: ', function(code) {
    //   rl.close();
    oauth2Client.getToken("4/TayRJ6LsCq43Pbhx223gu7VqcSqkyQipsyGCaoap99E", function (err, token) {
      if (err) {
        res.json({ "message": 'Error while trying to retrieve access token' + err });
        return;
      }
      oauth2Client.credentials = token;
      storeToken(token);
      callback(oauth2Client);
    });
    // });
  }

  /**
   * Store token to disk be used in later program executions.
   *
   * @param {Object} token The token to store to disk.
   */
  function storeToken(token) {
    try {
      fs.mkdirSync(TOKEN_DIR);
    } catch (err) {
      if (err.code != 'EEXIST') {
        throw err;
      }
    }
    fs.writeFile(TOKEN_PATH, JSON.stringify(token));
    console.log('Token stored to ' + TOKEN_PATH);
  }

  /**
   * Print the names and majors of students in a sample spreadsheet:
   * https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
   */
  //https://docs.google.com/spreadsheets/d/19G_UTLVOO1Ils-I809VMLjVyUCjlmBjPpOHygIQfs9I/edit#gid=1254190718
  //id	tmvdbID	title	popularity	release_date	subtitle	adamj6065@gmail.com	linhtu123aa@gmail.com	amvhuva002@gmail.com	amvhuva003	amvhuva004	amvhuva005	amvhuva006	xxx@gmail.com
  function listMajors(auth) {
    var sheets = google.sheets('v4');
    sheets.spreadsheets.values.get({
      auth: auth,
      spreadsheetId: '19G_UTLVOO1Ils-I809VMLjVyUCjlmBjPpOHygIQfs9I',
      range: sheetName + '!A1:S',
    }, function (err, response) {
      if (err) {
        res.json({ "message": 'The API returned an error: ' + err });
        return;
      }
      var data = response.values;
      if (data.length == 0) {
        res.json({ "message": 'No data found.' });
      } else {

        var fs = require('fs');
        var logStream = fs.createWriteStream('log.txt', { 'flags': 'w' });
        // use {'flags': 'a'} to append and {'flags': 'w'} to erase and write a new file
        var temp = 0;
        var lastMail = "unknow";
        for (var i = 0; i < data.length; i++) {
          var row = data[i];
          var upmail = row[0];
          var tmvdbID = parseInt(row[1]);
          var sub = row[2];
          var url = row[2];

          if (upmail == "") {
            upmail = lastMail;
          }
          lastMail = upmail;
          Movie.resetRequest(tmvdbID, function (result) {
            console.log("resetRequest" + tmvdbID);
          });

          var abc = {};
          abc.id = tmvdbID;
          if (isSubOnly == 0) {
            abc.url = url;
            abc.email = upmail;
            updateGGLinkTMDB(abc)
          }
          else {
            abc.subtitle = sub;
            updateSub(abc);
          }
        }
        res.json({ "message": 'Import Done!' });
      }
    });


    function updateGGLinkLocalID(abc) {
      var id = abc.id;
      var gglink = abc.url;
      var newEmail = abc.email;
      var subtitle = abc.subtitle;
      return Movie.getbyTMVDB(id, function (movie) {
        console.log('Begin insert for  tmvhdid : %d , email = %s , url = %s', id, newEmail, gglink);
        if (movie) {
          return GGLink.createOrUpdate(newEmail, gglink, [], function (gglinkObj) {
            if (gglinkObj) {
              if (movie.ggLinks.indexOf(gglinkObj._id) < 0) {
                movie.ggLinks.push(gglinkObj._id);
                movie.createdAt = Date.now();
                movie.requestCount = 0;
                movie.save(function (err, product, numAffected) {
                  console.log({ "code": 200, "message": "create new save GGLINK success" + numAffected });
                  return err;
                })

              }
            }
            else {
              console.log({ "code": 600, "message": "update gglink fail, link had already exists" });
              return null;
            }
          });
        }
        else {
          console.log({ "code": 500, "message": "Movie not exist, please try again!" })
          return 500;
        }
      });
    }

    function updateGGLinkTMDB(abc) {
      var id = abc.id;
      var gglink = abc.url;
      var newEmail = abc.email;
      var subtitle = abc.subtitle;
      return Movie.getbyTMVDB(id, function (movie) {
        console.log('Begin insert for  tmvhdid : %d , email = %s , url = %s', id, newEmail, gglink);
        if (movie) {
          return GGLink.createOrUpdate(newEmail, gglink, [], function (gglinkObj) {
            if (gglinkObj) {
              if (movie.ggLinks.indexOf(gglinkObj._id) < 0) {
                movie.ggLinks.push(gglinkObj._id);
                movie.createdAt = Date.now();
                movie.requestCount = 0;
                movie.save();
                console.log({ "code": 200, "message": "create new save GGLINK success" });
              }
            }
            else {
              console.log({ "code": 600, "message": "update gglink fail, link had already exists" });
            }
            return null;
          });
        }
        else console.log({ "code": 500, "message": "Movie not exist, please try again!" })
      });
    }

    function updateSub(abc) {
      var id = abc.id;
      var subtitle = abc.subtitle;
      return Movie.getbyTMVDB(id, function (movie) {
        if (movie) {
          if (subtitle.length > 0) {
            movie.subtitle_identify = subtitle;
            movie.save();
            console.log({ "code": 200, "message": "create new save SUBTITLE success" + id });
          }
          else {
            if (movie.subtitle.indexOf("https://photos.google.com") !== -1) {
              console.log({ "code": 200, "message": "REMOVE WRONG SUB = " + movie.subtitle });
              movie.subtitle_identify = "";
              movie.save();
            }
          }
        }
        else console.log({ "code": 500, "message": "Movie not exist, please try again!" })
      });
    }
  }
}

function exportCSV(req, res, next) {
  var count = req.headers.count || 2000;

  const { spawn } = require('child_process'),
    mongo = spawn('mongo', ['--eval', 'var count = ' + req.headers.count || 2000, 'localhost:27017/amvhuva', __dirname + '/../../export/exportmovies_popularity.js']);

  mongo.stdout.on('data', data => {
    console.log(`stdout: ${data}`);
  });

  mongo.stderr.on('data', data => {
    console.log(`stderr: ${data}`);
  });

  mongo.on('close', code => {
    console.log(`child process exited with code ${code}`);
    if (code == 0) {
      const mongoRestore = spawn('mongoexport', ['--host', 'localhost'
        , '--db', 'amvhuva'
        , '--collection', 'export.movie'
        , '--type=csv'
        , '--out', __dirname + '/movie.csv'
        , '--fields=tmvdbID,name,release_date,viewCount,requestCount,ggLinksLeng']);

      mongoRestore.stdout.on('data', data => {
        console.log(`stdout: ${data}`);
      });

      mongoRestore.stderr.on('data', data => {
        console.log(`stderr: ${data}`);
      });

      mongoRestore.on('close', code => {
        console.log(`mongoRestore child process exited with code ${code}`);
        var file = __dirname + '/movie.csv';
        res.download(file); // Set disposition and send it.
      });
    }
    else res.send("error!!!")
  });

}

function exportNoSubtitleCSV(req, res, next) {
  var count = req.headers.count || 2000;

  const { spawn } = require('child_process'),
    mongo = spawn('mongo', ['--eval', 'var count = ' + req.headers.count || 2000, 'localhost:27017/amvhuva', __dirname + '/../../export/exportmovies_no_subtitle.js']);

  mongo.stdout.on('data', data => {
    console.log(`stdout: ${data}`);
  });

  mongo.stderr.on('data', data => {
    console.log(`stderr: ${data}`);
  });

  mongo.on('close', code => {
    console.log(`child process exited with code ${code}`);
    if (code == 0) {
      const mongoRestore = spawn('mongoexport', ['--host', 'localhost'
        , '--db', 'amvhuva'
        , '--collection', 'export.movie'
        , '--type=csv'
        , '--out', __dirname + '/movie.csv'
        , '--fields=tmvdbID,name,release_date,viewCount,requestCount,ggLinksLeng']);

      mongoRestore.stdout.on('data', data => {
        console.log(`stdout: ${data}`);
      });

      mongoRestore.stderr.on('data', data => {
        console.log(`stderr: ${data}`);
      });

      mongoRestore.on('close', code => {
        console.log(`mongoRestore child process exited with code ${code}`);
        var file = __dirname + '/movie.csv';
        res.download(file); // Set disposition and send it.
      });
    }
    else res.send("error!!!")
  });

}

function exportCSVRequestCount(req, res, next) {
  var count = req.headers.count || 2000;

  const { spawn } = require('child_process'),
    mongo = spawn('mongo', ['--eval', 'var count = ' + req.headers.count || 2000, 'localhost:27017/amvhuva', __dirname + '/../../export/exportmovies_requestCount.js']);

  mongo.stdout.on('data', data => {
    console.log(`stdout: ${data}`);
  });

  mongo.stderr.on('data', data => {
    console.log(`stderr: ${data}`);
  });

  mongo.on('close', code => {
    console.log(`child process exited with code ${code}`);
    if (code == 0) {
      const mongoRestore = spawn('mongoexport', ['--host', 'localhost'
        , '--db', 'amvhuva'
        , '--collection', 'export.movie.requestCount'
        , '--type=csv'
        , '--out', __dirname + '/movie.csv'
        , '--fields=tmvdbID,name,release_date,viewCount,requestCount,ggLinksLeng']);

      mongoRestore.stdout.on('data', data => {
        console.log(`stdout: ${data}`);
      });

      mongoRestore.stderr.on('data', data => {
        console.log(`stderr: ${data}`);
      });

      mongoRestore.on('close', code => {
        console.log(`mongoRestore child process exited with code ${code}`);
        var file = __dirname + '/movie.csv';
        res.download(file); // Set disposition and send it.
      });
    }
    else res.send("error!!!")
  });

}

export default { importFromGGSheet, exportCSV, exportCSVRequestCount, exportNoSubtitleCSV };