import { ConnectableObservable } from 'rx';

var request = require('request');
var cheerio = require('whacko')

const PARSE_CODE = {
  SUCCESS: 0,
  FAILED: 1,
  NOT_READY: 2,
  UNKNOW: 3,
}


function parse(req, res, next) {
  var gglink = req.query.gglink;
  var code = "79468658" || req.query.code;

  var service = 'http://127.0.0.1:3002/api/ggparse?gglink=' + gglink;
  request(service, function (error, response, body) {

    if (error || response.statusCode == 500) {
      var result = { parseCode: PARSE_CODE.FAILED, data: null };
      return res.json(result);
    }

    if (response.statusCode == 200) {
      var result = { parseCode: PARSE_CODE.SUCCESS, data: JSON.parse(body) };
      return res.json(result);
    }

    if (response.statusCode == 300) {
      var result = { parseCode: PARSE_CODE.NOT_READY, data: JSON.parse(body) };
      return res.json(result);
    }
  });
}

function parse2(req, res, next) {
  var gglink = req.body.gglink;
  var code = "79468658" || req.body.code;
  var service = 'http://127.0.0.1:3002/api/ggparse?gglink=' + gglink;
  request(service, function (error, response, body) {
    if (error || response.statusCode == 500) {
      var result = { parseCode: PARSE_CODE.FAILED, data: null };
      return next(result);
    }

    if (response.statusCode == 200) {
      var result = { parseCode: PARSE_CODE.SUCCESS, data: JSON.parse(body) };
      return next(result);
    }

    if (response.statusCode == 300) {
      var result = { parseCode: PARSE_CODE.NOT_READY, data: JSON.parse(body) };
      return next(result);
    }
  });
}

function parse3(link, next) {
  var gglink = link;
  var code = "79468658" || req.body.code;

  var service = 'http://127.0.0.1:3002/api/ggparse?gglink=' + gglink;
  request(service, function (error, response, body) {

    if (error || response.statusCode == 404) {
      var result = { parseCode: PARSE_CODE.UNKNOW, data: null };
      return next(result);
    }

    if (response.statusCode == 500) {
      var result = { parseCode: PARSE_CODE.FAILED, data: null };
      return next(result);
    }

    if (response.statusCode == 200) {
      var result = { parseCode: PARSE_CODE.SUCCESS, data: JSON.parse(body) };
      return next(result);
    }

    if (response.statusCode == 300) {
      var result = { parseCode: PARSE_CODE.NOT_READY, data: JSON.parse(body) };
      return next(result);
    }
  });
}

function checkLinkDie(link, next) {
  var service = 'http://127.0.0.1:3002/api/checkdie?link=' + link;
  return request(service, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log("==================================== checkLinkDie ============" + response.statusCode);
      var status = JSON.parse(body);
      if (status && status.code >= 400) {
        return next(true);
      }
    }

    if (error || response.statusCode != 200) {
      console.log("==================================== 3002 die ============");
    }
    return next(false);
  });
}

function encodeStream(str) {
  var list = [];
  var streams = str.split("url=");
  streams.forEach(function (element) {
    if (element != "") {
      //console.log("parse =" + element);
      var params = getQueryParams(element);
      list.push(params);
    }
  }, this);

  return list;
}

function getQueryParams(qs) {
  var splits = decodeURIComponent(qs).split("&");
  var url = decodeURIComponent(splits[0]);
  var itag = decodeURIComponent(splits[1]).split("=")[1];
  var type = decodeURIComponent(splits[2]).split("=")[1];
  var quality = decodeURIComponent(splits[3]).split("=")[1];

  return { url: url, itag: itag, type: type, quality: quality };

}
export default { parse, parse2, parse3, checkLinkDie, PARSE_CODE };