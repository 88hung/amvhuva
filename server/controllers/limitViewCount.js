import { EILSEQ } from 'constants';
var request = require('request');
const requestIp = require('request-ip');
var async = require('async');

const BANNED_TTL = 60 * 60;
const CR_TTL = 60 * 60 * 5;
const LIMIT_VIEW = 1;

var redis = require("redis");
var schedQueueClient = redis.createClient();


function add(ip, movieID) {
    schedQueueClient.get(ip + movieID, function (err, reply) {
        if (reply) {
            schedQueueClient.incr(ip + movieID, function (err, value) { });
        }
        else {
            schedQueueClient.set(ip + movieID, 1, function (err, reply) {
                if (err == null)
                    schedQueueClient.expire(ip + movieID, BANNED_TTL, redis.print);
                console.log("New IP JOIN MOVIE = " + ip + "-" + movieID);
            })
        }
    })
}

function checkLimit(movieID, linkLeng, next) {
    return schedQueueClient.keys("*" + movieID, function (err, keys) {
        if (err || keys == null)
            return next(true);
        else {
            //console.log(`keys.length / linkLeng" + ${keys.length} / ${linkLeng} + " movieID = " + ${movieID}`);
            return next(keys.length / linkLeng <= LIMIT_VIEW);
        }
    })
}


function craw(movie, next) {
    var service = encodeURI(`http://127.0.0.1:3005/api/crawl?id=${movie.id}&name=${movie.title}&year=${movie.release_date.getFullYear()}`);
    console.log("BEGIN CRAWL = " + service);

    return request(service, function (error, response, body) {
        console.log("body" + body);
        if (!error && response.statusCode == 200) {
            var obj = JSON.parse(body);
            return next(obj);
        }
        if (error || response.statusCode != 200) {
            console.log("==================================== 3005 die ============");
        }
        return next(false);
    });
}


function getFromCrawler(movie, next) {
    return schedQueueClient.get("CR_" + movie.id, function (err, reply) {
        if (reply) {
            console.log("GET LINK CRAWLER = " + reply);
            return next(reply);
        }
        else {
            //request to crawler save and return 

            return craw(movie, function (obj) {
                if (obj) {
                    schedQueueClient.set("CR_" + movie.id, obj.url, 'EX', CR_TTL, function (err, reply) {
                        console.log("NEW LINK CRAWLER = " + obj.url);
                    })

                    return next(obj.url);
                }
                return next(null);
            })
        }
    });
}
export default {
    add,
    checkLimit,
    getFromCrawler
};