const express = require('express')
const app = express()


var request = require('request');
var cheerio = require('cheerio')


function parse(req, res,next) {
    var gglink = req.query.gglink;
    var code = "79468658" || req.query.code;

    request(gglink, function (error, response, html) {
      if (!error && response.statusCode == 200) {
        var $ = cheerio.load(html);
  
        var list_AF_initDataCallback = $('body > script').map(function (i, x) {
          return x.children[0];
        }).filter(function (i, x) {
          return x && x.data.match(/AF_initDataCallback/);
        }).toArray();
  
        list_AF_initDataCallback.forEach(function (element) {
          if (element) {
            var scriptText = element.data.replace(/\n/g, "");
            if (scriptText.search(code) > -1) {
              if (scriptText.search(code) > -1) {
                var listLinkStr = /return(.*]}])/.exec(scriptText);
                if(listLinkStr)
                {
                  var jsonString = listLinkStr[0].replace("return ", "");
                  var sources = JSON.parse(jsonString);
                  var data = sources[sources.length - 1];
                  if(data && data[code] && data[code].length > 0){
                    return res.json(encodeStream(data[code][0][0][2]));
                  }
                  else
                  {
                    console.log("===============================================link not ready====================================");
					console.log("===========" + gglink);
					return res.status(300).send([]);
                  }
                }
                else
                {
                  console.log("===============================================link not ready====================================");
                  console.log("===========" + gglink);
  
                  res.json({ message: "parseError" });
                }
              }
            }
    }
        }, this);
      } else return res.json({ message: "parseError" });
    });  
}

function encodeStream(str) {
  var list = [];
  var streams = str.split("url=");
  streams.forEach(function(element) {
    if(element != "")
    {
      //console.log("parse =" + element);
      var params = getQueryParams(element);
      list.push(params);
    }
  }, this);

  return list;
}

function getQueryParams(qs) {
  var splits = decodeURIComponent(qs).split("&");
  var url = decodeURIComponent(splits[0]);
  var itag = decodeURIComponent(splits[1]).split("=")[1];
  var type = decodeURIComponent(splits[2]).split("=")[1];
  var quality = decodeURIComponent(splits[3]).split("=")[1];

  return {url : url, itag : itag ,type : type, quality : quality};
  
}

app.get('/api/ggparse', (req, res) => {
    parse(req,res,null);
})

app.listen(3003, () => console.log('Example app listening on port 3003!'))
